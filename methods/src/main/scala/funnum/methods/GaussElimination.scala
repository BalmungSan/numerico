package funnum.methods

//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec

/**
  * given the N*N matrix, this method ''solves'' the system,
  */
object GaussElimination {
  /**
    * @param a Matrix that is going to apply the method
    */
  def apply(a: Matrix): Vect = {
    //get the size of the matrix
    val n = a.length

    //functional implementation of the iterative Gauss Elimination algorithm
    /**
      * @param a Matrix that is going to apply the method
      * @param k current iteration
      * @return x,  computed solution for the system
      */
    @tailrec
    def eliminate(a: Matrix, k: Int): Matrix = {
      val pivot = a(k)
      val akk = a(k)(k)
      val newMatrix = for {
        (row, i) <- a.zipWithIndex
        newRow = if (i <= k) row else {
          val m = row(k) / akk
          (row zip pivot) map { case(aij, pj) => aij - (pj * m) }
        }
      } yield newRow
      if (k == (n-2)) newMatrix
      else eliminate(newMatrix, k+1)
    }

    //perform row reduction of the matrix
    val reduced = eliminate(a, 0)

    //solve the equation system using the reduced matrix
    val x = Array.ofDim[Double](n)
    for (k <- (n-1) to 0 by -1) {
      val acc = for {
        p <- (n-1) until k by -1
      } yield reduced(k)(p) * x(p)
      x(k) = (reduced(k)(n) - acc.sum) / reduced(k)(k)
    }

    //return the computed solution of the system
    x
  }
}
