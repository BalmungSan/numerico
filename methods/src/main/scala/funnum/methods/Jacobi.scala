package funnum.methods

//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec

/**
  * This method attempts to find a solution for a system of
  * linear equations, generating a first aproximation, and
  * based on that first aproximation , calculates the others
  *
  * */
object Jacobi {

  /** @param a Matrix of coefficients
    * @param initial Vector with the initial values for the method
    * @param b Vector of idepentend terms
    * @param r Relaxation   (tries to improve the convergence of the method)
    *          by default is 1;
    * @param tolerance the maximum error that the algorithm will tolerate to return a value as a root,
    * '''must''' be ''equal or greater than zero''
    * @param maxSteps the maximum number of iterations to perform before halt the execution,
    * '''must''' be a ''positive number''
    * @param absError flag to determine which error to use,
    * ''if true then'' '''absolute error''' will be used, ''else'' '''relative error'''
    * @throws java.lang.IllegalArgumentException
    * if the value is malformed  or
    * if the value is a root or
    * if the tolerance is negative or
    * if the maximum number of iterations is less or equal than zero
    * @throws java.lang.AssertionError
    * if the maximum number of iterations is reached before finding a root
    *
    * @see [[scala.annotation.tailrec @tailrec]]
    */

  //create a ResultsTable for this method
  var table = ResultsTable("n", "x","popo", "Error")


  @throws(classOf[AssertionError])
  @throws(classOf[IllegalArgumentException])
  def apply(a: Matrix, initial: Vect, b: Vect, r: Double = 1.0d)
    (maxSteps: Int, tolerance: Double, absError: Boolean = true): Vect = {

    //check preconditions
    require(tolerance >= 0.0, "the tolerance can not be negative")
    require(maxSteps > 0, "the maximum number of iterations must be a number greater than zero")
    val n = a.length
    require(n == initial.length && n == b.length, "bad dimensions of the input")
    require(r > 0, "relaxation value must be greater tan 0")
    val r1 = 1.0d - r
    val params = for {
      i <- 1 to n
    } yield s"x${i}"
    println(params)
    val parameters = Vector("n")++params++Vector("Error")
    table = ResultsTable(parameters : _*)

    //functional implementation of an iterative process using a tail recursive function
    @tailrec
    def iterate(xp: Vect, xpNorm: Double, iter: Int): Vect = {
      //verify that the maximum number of iterations has not been reached yet
      assert(iter < maxSteps, "the execution was halted because the maximum number of steps were exceeded")

      //compute the new solution
      val xa: Vect = for {
        i <- (0 until n).toArray
        acc: Seq[Double] = for {
          j <- 0 until n
          if j != i
        } yield a(i)(j) * xp(j)
        act: Double = (b(i) - acc.sum) / a(i)(i)
      } yield ((r * act) + (r1 * xp(i)))

      //compute the norm of the actual solution
      val xaNorm = norm(xa)

      //compute the error of the actual solution
      val error = if (absError) absoluteError(xaNorm, xpNorm) else relativeError(xaNorm, xpNorm)

      //registart en la table
      val row = Vector(iter)++xa++Vector(error)
      table(row : _*)

      //check if the error associated with the actual solution is less than the tolerance
      //if yes, return the actual solution
      //otherwise, iterate
      if (error < tolerance) xa
      else iterate(xa, xaNorm, iter + 1)
    }

    //iterate until a solution is found or the maximum number of iterations is reached
    iterate(initial, norm(initial), 0)
  }
}
