package funnum.methods

//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec

/**
  * this method ''decompose'' a given Matrix in 2
  * L that is a lower triangular matrix and U that is
  * a superior triangular matrix
  * */
object LUDecomposition {
  /**
    * @param factAlgorithm Generalization of the algorithm thas is going
    *                      to be apply for de decomposition
    *  */
  def apply(factAlgorithm: FactorizationAlgorithm)
    (a: Matrix, bs: Seq[Vect]): Seq[Vect] = {
    val (l, u) = factAlgorithm(a)
    val detL = det(l)
    val detU = det(u)
    val detA = detL * detU

    for {
      b <- bs
      z = progressiveSubstitution(l, b)
      x = regressiveSubtitution(u, z)
    } yield x
  }

  /**
    * @param m given the matrix return the determinat of that matrix
    * */
  private def det(m: Matrix): Double = {
    val n = m.length
    val diagonal = for {
      k <- 0 until n
    } yield m(k)(k)
    diagonal.fold (1d) { case(acc, x) => acc * x }
  }

  /**
    * this method replace the values found, from top to bottom
    * @param l lower triangular matrix ,
    * @param b vector with the independent terms of the system of
    *          linear equations
    * */
  private def progressiveSubstitution(l: Matrix, b: Vect): Vect = {
    val n = l.length
    val z = Array.ofDim[Double](n)

    for (k <- 0 until n) {
      val acc = for {
        p <- 0 until k
      } yield l(k)(p) * z(p)
      z(k) = (b(k) - acc.sum) / l(k)(k)
    }

    //return the calculated z vector
    z
  }

  /**
    * this method replace the values found, from bottom to top
    * @param u upper triangular matrix ,
    * @param z vector with the independent terms of the system of
    *          linear equations
    * */ 
  private def regressiveSubtitution(u: Matrix, z: Vect): Vect = {
    val n = u.length
    val x = Array.ofDim[Double](n)

    for (k <- (n-1) to 0 by -1) {
      val acc = for {
        p <- (n-1) until k by -1
      } yield u(k)(p) * x(p)
      x(k) = (z(k) - acc.sum) / u(k)(k)
    }

    //return the calculated x vector
    x
  }
}

/** */
sealed trait FactorizationAlgorithm {
  /** */
  def apply(a: Matrix): (Matrix, Matrix)
}

/**  */
case object Cholesky extends FactorizationAlgorithm {
  //importing the scala math package for using the square root function
  import scala.math

  /**
    * the diagonal beetween L and U are the same
    *
    *
    * @param a matrix to apply the decomposition
    * @return L and U , corresponding the lower an upper triangular matrixes
    * */
  override def apply(a: Matrix): (Matrix, Matrix) = {
    val n = a.length
    val l = Array.ofDim[Double](n, n)
    val u = Array.ofDim[Double](n, n)

    for (k <- 0 until n) {
      //compute the k determinant
      val acc = for {
        p <- 0 until k
      } yield l(k)(p) * u(p)(k)
      val d = math.sqrt(a(k)(k) - acc.sum)

      //assing the diagonals value
      l(k)(k) = d
      u(k)(k) = d

      //filling the k column of L
      for (i <- (k+1) until n) {
        val acc = for {
          p <- 0 until k
        } yield l(i)(p) * u(p)(k)
        l(i)(k) = (a(i)(k) - acc.sum) / d
      }

      //filling the k row of U
      for (j <- (k+1) until n) {
        val acc = for {
          p <- 0 until k
        } yield l(k)(p) * u(p)(j)
        u(k)(j) = (a(k)(j) - acc.sum) / d
      }
    }

    //return the computed L and U matrices
    (l, u)
  }
}

case object Doolittle extends FactorizationAlgorithm {
  /**
    * the Lower matrix has his diagonal full of ones
    * @param a Matrix to apply  the method
    * */
  override def apply(a: Matrix): (Matrix, Matrix) = {
    val n = a.length
    val l = Array.ofDim[Double](n, n)
    val u = Array.ofDim[Double](n, n)

    for (k <- 0 until n) {
      //compute the k determinant
      val acc = for {
        p <- 0 until k
      } yield l(k)(p) * u(p)(k)
      val d = a(k)(k) - acc.sum

      //assing the diagonals value
      l(k)(k) = 1
      u(k)(k) = d

      //filling the k column of L
      for (i <- (k+1) until n) {
        val acc = for {
          p <- 0 until k
        } yield l(i)(p) * u(p)(k)
        l(i)(k) = (a(i)(k) - acc.sum) / d
      }

      //filling the k row of U
      for (j <- (k+1) until n) {
        val acc = for {
          p <- 0 until k
        } yield l(k)(p) * u(p)(j)
        u(k)(j) = a(k)(j) - acc.sum
      }
    }

    //return the computed L and U matrices
    (l, u)
  }
}

case object Crout extends FactorizationAlgorithm {
  /**
    * * the Upper matrix has his diagonal full of ones
    * @param a Matrix to apply  the method
    * */
  override def apply(a: Matrix): (Matrix, Matrix) = {
    val n = a.length
    val l = Array.ofDim[Double](n, n)
    val u = Array.ofDim[Double](n, n)

    for (k <- 0 until n) {
      //compute the k determinant
      val acc = for {
        p <- 0 until k
      } yield l(k)(p) * u(p)(k)
      val d = a(k)(k) - acc.sum

      //assing the diagonals value
      l(k)(k) = d
      u(k)(k) = 1

      //filling the k column of L
      for (i <- (k+1) until n) {
        val acc = for {
          p <- 0 until k
        } yield l(i)(p) * u(p)(k)
        l(i)(k) = a(i)(k) - acc.sum
      }

      //filling the k row of U
      for (j <- (k+1) until n) {
        val acc = for {
          p <- 0 until k
        } yield l(k)(p) * u(p)(j)
        u(k)(j) = (a(k)(j) - acc.sum) / d
      }
    }

    //return the computed L and U matrices
    (l, u)
  }
}

case object GaussFactorization extends FactorizationAlgorithm {
  /**
    * Given a matrix, this method returns the factorization of the
    * matrix in a L (lower triangular matrix) and U(upper triangular matrix)
    * finding the correspondig multipliers of each row
    *
    * @param a Matrix to decompose
    *
   */

  override def apply(a: Matrix): (Matrix, Matrix) = {
    val n = a.length
    val _a = a.map(_.clone)
    val l = Array.ofDim[Double](n, n)
    val u = Array.ofDim[Double](n, n)

    for (i <- 0 until n) l(i)(i) = 1
    for (j <- 0 until n) u(0)(j) = _a(0)(j)
    for (k <- 0 until (n-1)) {
      for (i <- (k+1) until n) {
        val m = _a(i)(k) / _a(k)(k)
        l(i)(k) = m
        for (j <- 0 until n) {
          _a(i)(j) = _a(i)(j) - (m * _a(k)(j))
          if (i == k+1) u(i)(j) = _a(i)(j)
        }
      }
    }

    //return the computed L and U matrices
    (l, u)
  }
}
