package funnum.methods

//import a mutable array
import scala.collection.mutable.ArrayBuffer

//import jquery for creating the HTML5 table
import org.scalajs.jquery.{jQuery, JQuery}

/** Results table for storing the intermediate values of each iteration of the methods */
class ResultsTable private(private[this] val columns: Seq[String]) {
  /** Array to hold the rows of the table */
  private[this] val data = ArrayBuffer.empty[Seq[String]]

  /** Adds a new row to the table */
  private[methods] def apply(row: Any*): Unit = {
    require(row.length == columns.length, "Bad number of columns")
    val formattedRow = (columns zip row)map {
      case ("n", n: Int) => n.toString
      case ("Error", "-") => "-"
      case ("Error", error: Double) => f"$error%1.0E"
      case (name: String, x: Double) if name.startsWith("x") => f"$x%1.15f"
      case (_, x: Double) => f"$x%1.4f"
    }
    data += formattedRow
  }

  /** Clears the table */
  private[methods] def clear(): Unit = data.clear()

  /** Returns an HTML5 table containing the data of this table */
  def toHTML5: JQuery = {
    //creates a new empty table
    val table = jQuery("<table style='width:100%'>")

    //add the header to the table
    val tableHeader = jQuery("<tr>")
    for (col <- columns) {
      tableHeader.append(jQuery(s"<th>$col</th>"))
    }
    table.append(tableHeader)

    //add each row to the table
    for (row <- data) {
      val tableRow = jQuery("<tr>")
      for (cell <- row) {
        tableRow.append(jQuery(s"<td>$cell</td>"))
      }
      table.append(tableRow)
    }

    //returns the generated table
    table
  }
}

/** Factory of ResultsTable */
private[methods] object ResultsTable {
  /** Creates a new table for a list of columns names */
  private[methods] def apply(columns: String*): ResultsTable = new ResultsTable(columns)
}
