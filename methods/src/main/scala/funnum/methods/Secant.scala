package funnum.methods

//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec

/**
  * This method attempts to find a root for the ''function'',
  * starting at a value(if the value is close to the root, better),
  * the open method uses a formula to predict the root,which consists
  * of finding tangents of points of the curve, so generally where it
  * cross with the x- axis is the an approximation of the root, but the
  * derivative is not necessary, instead uses a divided difference
  *
  *{{{
  *  val xa = x1 - ((y1 * (x1 - x0))/den)
  *  val ya = f(xa)
  *  val error = if (absError) absoluteError(xa, x1) else relativeError(xa, x1)
  *}}}
  *
  * @note this method '''assumes''' that the ''function'' is __continuous__
  * in the value where the search will be applied
  * @note this method is '''only designed''' for __one variable__ ''functions''
  * @see for a good initial value see [[funnum.methods.IncrementalSearch IncrementalSearch]]
  *
  */
object Secant {
  //create a ResultsTable for this method
  val table = ResultsTable("n", "x", "f(x)", "Error")

  /**
    * @param f the ''function'' to which the method will be applied
    * @param x0 lower bound
    * @param x1 upper bound           
    * @param tolerance the maximum error that the algorithm will tolerate to return a value as a root,
    * '''must''' be ''equal or greater than zero''
    * @param maxSteps the maximum number of iterations to perform before halt the execution,
    * '''must''' be a ''positive number''
    * @param absError flag to determine which error to use,
    * ''if true then'' '''absolute error''' will be used, ''else'' '''relative error'''
    *
    * @note a tolerance of zero force the algorithm to found a value
    * that is ''exactly'' a '''root''', not an ''approximation''.
    * @note this method can be __partially applied__ for a given '''function''',
    * and then calling the method multiple times for the same function with different inputs
    * @note the internal recursive function its optimized
    * using the [[scala.annotation.tailrec @tailrec annotation]]
    *
    * @throws java.lang.IllegalArgumentException
    * if the value is malformed  or
    * if the value is a root or
    * if the tolerance is negative or
    * if the maximum number of iterations is less or equal than zero
    * @throws java.lang.AssertionError
    * if the maximum number of iterations is reached before finding a root
    *
    * @see [[scala.annotation.tailrec @tailrec]]
    *
    *
    *
    *
    *
    */
  @throws(classOf[AssertionError])
  @throws(classOf[IllegalArgumentException])
  def apply(f: Double => Double)
    (x0: Double, x1: Double, tolerance: Double, maxSteps: Int, absError: Boolean = true): Double = {
    //clear the table for a new empty execution
    table.clear()

    //check preconditions
    require(tolerance >= 0.0, "the tolerance can not be negative")
    require(maxSteps > 0, "the maximum number of iterations must be a number greater than zero")

    //functional implementation of an iterative process using a tail recursive function
    @tailrec
    def iterate(x0: Double, y0: Double, x1: Double, y1: Double, iter: Int): Double = {
      //verify that the maximum number of iterations has not been reached yet
      assert(iter < maxSteps, "the execution was halted because the maximum number of steps were exceeded")

      //check that the denominator is not zero
      val den = y1 - y0
      assert(den != 0, "The execution was halted because there is a possible multiple root")

      //compute the new value and the error associated with it
      val xa = x1 - ((y1 * (x1 - x0))/den)
      val ya = f(xa)
      val error = if (absError) absoluteError(xa, x1) else relativeError(xa, x1)

      //register in the table the iteration values
      table(iter, xa, ya, error)

      //if the actual value is a root
      //or if the error is less than the tolerance
      //return the actual value as a root
      if (ya == 0.0 || error < tolerance) xa
      //otherwise iterate with the new value
      else iterate(x1, y1, xa, ya, iter + 1)
    }

    //compute the first value
    val y0 = f(x0)
    val y1 = f(x1)

    //register in the table the first iteration of the method
    table(0, x0, y0, "-")
    table(1, x1, y1, "-")

    //check if any of the initial values is a root, if yes return it
    if (y0 == 0) x0
    if (y1 == 0) x1
    //else, iterate until a root is found,
    //or the maximum number of iterations is reached
    //or the denominator tends to zero
    else iterate(x0, y0, x1, y1, 2)
  }
}
