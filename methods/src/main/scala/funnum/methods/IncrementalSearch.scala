package funnum.methods

//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec

/** The Incremental Search Method for finding an interval that contains a root of a ''function''
  *
  * This method attempts to find a interval containing a root for the ''function'',
  * starting at a given arbitrary point, the method iterates by computing
  * a new point using a given delta and verifing the follow condition:
  *
  * F(X,,i,,) * F(X,,i+1,,) < 0
  *
  * if the condition is met then the returned interval is [X,,i,,, X,,i+1,,]
  *
  * @note this method '''assumes''' that the ''function'' is __continuous__
  * in the interval where the search will be applied
  * @note this method is '''only designed''' for __one variable__ ''functions''
  */
object IncrementalSearch {
  //create a ResultsTable for this method
  val table = ResultsTable("n", "x", "f(x)")

  /** Computes an interval that holds a '''root''' of a ''function'' using a given initial point and a delta
    *
    * The iterative part of the algorithm is implemented using an internal tail recursive function
    * that computes each new value as the sum of the previous value with the delta and checks
    * if the two make an interval that contains a root, if not the function is recursively
    * called until an interval is found
    *
    * @param f the ''function'' to which the method will be applied
    * @param initial the initial point
    * @param stepSize the delta, '''must''' be different of ''zero''
    * @param maxSteps the maximum number of iterations to perform before halt the execution,
    * '''must''' be a ''positive number''
    * @return the computed interval as [[scala.Tuple2 tuple]] of the form (lowerBound, upperBound)
    *
    * @note '''if a root is found''' during the execution of the method
    * then the returned value is a tuple of the form (root, root)
    * @note this method can be __partially applied__ for a given '''function''',
    * and then calling the method multiple times for the same function with different inputs
    * @note the internal recursive function its optimized
    * using the [[scala.annotation.tailrec @tailrec annotation]]
    *
    * @throws java.lang.IllegalArgumentException
    * if the stepSize is zero or
    * if the maximum number of iterations is less or equal than zero or
    * if the initial value is a root of the function
    * @throws java.lang.AssertionError
    * if the maximum number of iterations is reached before finding an interval
    *
    * @see [[scala.annotation.tailrec @tailrec]]
    */
  @throws(classOf[AssertionError])
  @throws(classOf[IllegalArgumentException])
  def apply(f: Double => Double)
    (initial: Double, stepSize: Double, maxSteps: Int): (Double, Double) = {
    //clear the table for a new empty execution
    table.clear()

    //check preconditions
    require(stepSize != 0.0, "the step size must be a number different than zero")
    require(maxSteps > 0, "the maximum number of iterations must be a number greater than zero")

    //functional implementation of an iterative process using a tail recursive function
    @tailrec
    def iterate(xp: Double, yp: Double, iter: Int): (Double, Double) = {
      //verify that the maximum number of iterations has not been reached yet
      assert(iter < maxSteps, "the execution was halted because the maximum number of steps were exceeded")

      //compute the new X and the value of applying the function to it
      val xa = xp + stepSize
      val ya = f(xa)

      //register in the table the iteration values
      table(iter, xa, ya)

      //check if the actual value is a root of the function
      if (ya == 0.0) (xa, xa)
      //check if the previous value and the new value conform and interval
      //that contains the root of the function, if yes return the interval
      else if (yp * ya < 0.0) sortedPair(xp, xa)
      //if not iterate
      else iterate(xa, ya, iter + 1)
    }

    //compute the first value
    val yi = f(initial)

    //register in the table the first iteration of the method
    table(0, initial, yi)

    //check if the initial value is a root, if yes return an interval of it to it
    if (yi == 0) (initial, initial)
    //else, iterate until the interval is found or until the maximum number of iterations is reached
    else iterate(initial, yi, 1)
  }

  //Returns a sorted tuple from two elements
  private[this] def sortedPair(a: Double, b: Double): (Double, Double) = {
    if (a < b) (a, b)
    else (b, a)
  }
}
