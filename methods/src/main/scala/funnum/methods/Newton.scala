package funnum.methods

//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec

/**
  *
  * This method attempts to find a root for the ''function'',
  * starting at a value(if the value is close to the root, better),
  * the open method uses a formula to predict the root,which consists
  * of finding tangents of points of the curve, so generally where it
  * cross with the x- axis is the an approximation of the root
  *
  *
  *{{{
  * val xa = xp - (yp/dyp)
  * val ya = f(xa)
  * val dya = df(xa)
  * val error = if (absError) absoluteError(xa, xp) else relativeError(xa, xp)
  *}}}
  *
  * @note this method '''assumes''' that the ''function'' is __continuous__
  * in the value where the search will be applied
  * @note this method is '''only designed''' for __one variable__ ''functions''
  * @see for a good initial value see [[funnum.methods.IncrementalSearch IncrementalSearch]]
  */
object Newton {
  //create a ResultsTable for this method
  val table = ResultsTable("n", "x", "f(x)","f'(x)", "Error")

  /**
    *
    * @param f the ''function'' to which the method will be applied
    * @param df ''derivate'' of the ''function'' used to calculate de tangent
    * @param initial initial value for the algorithm to start
    * @param tolerance the maximum error that the algorithm will tolerate to return a value as a root,
    * '''must''' be ''equal or greater than zero''
    * @param maxSteps the maximum number of iterations to perform before halt the execution,
    * '''must''' be a ''positive number''
    * @param absError flag to determine which error to use,
    * ''if true then'' '''absolute error''' will be used, ''else'' '''relative error'''
    *
    * @note a tolerance of zero force the algorithm to found a value
    * that is ''exactly'' a '''root''', not an ''approximation''.
    * @note this method can be __partially applied__ for a given '''function''',
    * and then calling the method multiple times for the same function with different inputs
    * @note the internal recursive function its optimized
    * using the [[scala.annotation.tailrec @tailrec annotation]]
    *
    * @throws java.lang.IllegalArgumentException
    * if the value is malformed  or
    * if the value is a root or
    * if the tolerance is negative or
    * if the maximum number of iterations is less or equal than zero
    * @throws java.lang.AssertionError
    * if the maximum number of iterations is reached before finding a root
    *
    * @see [[scala.annotation.tailrec @tailrec]]
    *
    */
  @throws(classOf[AssertionError])
  @throws(classOf[IllegalArgumentException])
  def apply(f: Double => Double)
    (df: Double => Double)
    (initial: Double, tolerance: Double, maxSteps: Int, absError: Boolean = true): Double = {
    //clear the table for a new empty execution
    table.clear()

    //check preconditions
    require(tolerance >= 0.0, "the tolerance can not be negative")
    require(maxSteps > 0, "the maximum number of iterations must be a number greater than zero")

    //functional implementation of an iterative process using a tail recursive function
    @tailrec
    def iterate(xp: Double, yp: Double, dyp: Double, iter: Int): Double = {
      //verify that the maximum number of iterations has not been reached yet
      assert(iter < maxSteps, "the execution was halted because the maximum number of steps were exceeded")

      //check that the derivative of x is not zero
      assert(dyp != 0, "The execution was halted because there is a possible multiple root")

      //compute the new value and the error associated with it
      val xa = xp - (yp/dyp)
      val ya = f(xa)
      val dya = df(xa)
      val error = if (absError) absoluteError(xa, xp) else relativeError(xa, xp)

      //register in the table the iteration values
      table(iter, xa, ya, dya, error)

      //if the actual value is a root
      //or if the error is less than the tolerance
      //return the actual value as a root
      if (ya == 0.0 || error < tolerance) xa
      //otherwise iterate with the new value
      else iterate(xa, ya, dya, iter + 1)
    }

    //compute the first values
    val yi = f(initial)
    val dyi = df(initial)

    //register in the table the first iteration of the method
    table(0, initial, yi, dyi, "-")

    //check if the initial value is a root, if yes return it
    if (yi == 0) initial
    //else, iterate until a root is found,
    //or the maximum number of iterations is reached
    //or the derivative tends to zero
    else iterate(initial, yi, dyi, 1)
  }
}
