package funnum.methods

//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec

/**
  * This method attempts to find a root for the ''function'',
  * starting at a value(if the value is close to the root, better),
  * the open method uses a formula to predict the root, fix the equation
  * f(x) = 0 so x is in the rigth side of the equation, x = g(x)
  *
  *
  *{{{
  * val xa = g(xp)
  * val ya = f(xa)
  * val error = if (absError) absoluteError(xa, xp) else relativeError(xa, xp)
  *}}}
  *
  * @note this method '''assumes''' that the ''function'' is __continuous__
  * in the value where the search will be applied
  * @note this method is '''only designed''' for __one variable__ ''functions''
  * @note this method is '''assumes''' that g(x) is a correct equation of x, based on the f(x)
  * @see for a good initial value see [[funnum.methods.IncrementalSearch IncrementalSearch]]
  */
object FixedPoint {
  //create a ResultsTable for this method
  val table = ResultsTable("n", "x", "f(x)", "Error")

  /**
    * Returns a '''root''' of a ''function'' using a given interval and a initial value
    *
    * The iterative part of the algorithm is implemented using an internal tail recursive function
    * that computes the root with f(x) and g(x), if not the function is recursively called until a
    * '''root''' is found
    *
    * @param f the ''function'' to which the method will be applied
    * @param g the ''function'' to which the method will be applied
    * @param initial initial value for the algorithm to start
    * @param tolerance the maximum error that the algorithm will tolerate to return a value as a root,
    * '''must''' be ''equal or greater than zero''
    * @param maxSteps the maximum number of iterations to perform before halt the execution,
    * '''must''' be a ''positive number''
    * @param absError flag to determine which error to use,
    * ''if true then'' '''absolute error''' will be used, ''else'' '''relative error'''
    *
    * @note a tolerance of zero force the algorithm to found a value
    * that is ''exactly'' a '''root''', not an ''approximation''.
    * @note this method can be __partially applied__ for a given '''function''',
    * and then calling the method multiple times for the same function with different inputs
    * @note the internal recursive function its optimized
    * using the [[scala.annotation.tailrec @tailrec annotation]]
    *
    * @throws java.lang.IllegalArgumentException
    * if the value is malformed  or
    * if the value is a root or
    * if the tolerance is negative or
    * if the maximum number of iterations is less or equal than zero
    * @throws java.lang.AssertionError
    * if the maximum number of iterations is reached before finding a root
    *
    * @see [[scala.annotation.tailrec @tailrec]]
    *
    */
  @throws(classOf[AssertionError])
  @throws(classOf[IllegalArgumentException])
  def apply(f: Double => Double)
    (g: Double => Double)
    (initial: Double, tolerance: Double, maxSteps: Int, absError: Boolean = true): Double = {
    //clear the table for a new empty execution
    table.clear()

    //check preconditions
    require(tolerance >= 0.0, "the tolerance can not be negative")
    require(maxSteps > 0, "the maximum number of iterations must be a number greater than zero")

    //functional implementation of an iterative process using a tail recursive function
    @tailrec
    def iterate(xp: Double,iter: Int): Double = {
      //verify that the maximum number of iterations has not been reached yet
      assert(iter < maxSteps, "the execution was halted because the maximum number of steps were exceeded")

      //compute the new value and the error associated with it
      val xa = g(xp)
      val ya = f(xa)
      val error = if (absError) absoluteError(xa, xp) else relativeError(xa, xp)

      //register in the table the iteration values
      table(iter, xa, ya, error)

      //if the actual value is a root
      //or if the error is less than the tolerance
      //return the actual value as a root
      if (ya == 0.0 || error < tolerance) xa
      //otherwise iterate with the new value
      else iterate(xa ,iter + 1)
    }

    //compute the first value
    val yi = f(initial)

    //register in the table the first iteration of the method
    table(0, initial, yi, "-")

    //check if the first value is a root, if yes return it
    if (yi == 0) initial
    //else, iterate until a root is found or the maximum number of iterations is reached
    else iterate(initial, 1)
  }
}
