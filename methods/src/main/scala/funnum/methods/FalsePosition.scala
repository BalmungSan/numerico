package funnum.methods

//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec

/** The false Position for finding a root of a ''function''
  *
  * This method attempts to find a root for the ''function'',
  * starting at a given interval that contains at least one root,
  * the method consist in join f(xl) and f(xu) with a straight line
  * the intersection of this line with the x-axis is a root of the
  * function, or it is close enough.
  *
  *
  * {{{
  * val xm = a - ((ya * (b - a)) / (yb - ya))
  * val error = if (absError) absoluteError(xm, xp) else relativeError(xm, xp)
  * if (ym == 0.0 || error < tolerance) xm
  * }}}
  *
  * @note if you don't have an interval that contains a root of the function,
  * you can use the [[funnum.methods.IncrementalSearch Incremental Search Method]]
  * implemented in this api to find one
  * @note this method '''assumes''' that the ''function'' is __continuous__
  * in the interval where the search will be applied
  * @note this method is '''only designed''' for __one variable__ ''functions''
  *
  * @see [[funnum.methods.IncrementalSearch IncrementalSearch]]
  *
  */
object FalsePosition {
  //create a ResultsTable for this method
  val table = ResultsTable("n", "xl", "xu", "xm", "f(xm)", "Error")

  /**
    * Returns a '''root''' of a ''function'' using a given interval and a tolerance
    *
    * The iterative part of the algorithm is implemented using an internal tail recursive function
    * that computes the intersection with the x axis and checks if the  intersection value is a root,
    * if not the function is recursively called until a '''root''' is found
    *
    * @param f the ''function'' to which the method will be applied
    * @param interval a [[scala.Tuple2 tuple]] of the form (lower bound, upper bound)
    * that will be used as the initial interval for the algorithm,
    * '''must''' contains at least ''one root''
    * @param tolerance the maximum error that the algorithm will tolerate to return a value as a root,
    * '''must''' be ''equal or greater than zero''
    * @param maxSteps the maximum number of iterations to perform before halt the execution,
    * '''must''' be a ''positive number''
    * @param absError flag to determine which error to use,
    * ''if true then'' '''absolute error''' will be used, ''else'' '''relative error'''
    *
    * @note a tolerance of zero force the algorithm to found a value
    * that is ''exactly'' a '''root''', not an ''approximation''.
    * @note this method can be __partially applied__ for a given '''function''',
    * and then calling the method multiple times for the same function with different inputs
    * @note the internal recursive function its optimized
    * using the [[scala.annotation.tailrec @tailrec annotation]]
    *
    * @throws java.lang.IllegalArgumentException
    * if the interval is malformed or does not contains a root or
    * if any bound of the interval is a root or
    * if the tolerance is negative or
    * if the maximum number of iterations is less or equal than zero
    * @throws java.lang.AssertionError
    * if the maximum number of iterations is reached before finding a root
    *
    * @see [[scala.annotation.tailrec @tailrec]]
    *
    */
  @throws(classOf[AssertionError])
  @throws(classOf[IllegalArgumentException])
  def apply(f: Double => Double)
    (interval: (Double, Double), tolerance: Double, maxSteps: Int, absError: Boolean = true): Double = {
    //clear the table for a new empty execution
    table.clear()

    //check preconditions
    val (a, b) = interval
    val ya = f(a)
    val yb = f(b)
    require(a < b, "the interval is malformed")
    require(ya * yb < 0.0, "the interval does not contains a root")
    require(ya != 0.0, "the lower bound is a root")
    require(yb != 0.0, "the upper bound is a root")
    require(tolerance >= 0.0, "the tolerance can not be negative")
    require(maxSteps > 0, "the maximum number of iterations must be a number greater than zero")

    //functional implementation of an iterative process using a tail recursive function
    @tailrec
    def iterate(a: Double, b: Double, ya: Double, yb: Double, xp: Double, iter: Int): Double = {
      //verify that the maximum number of iterations has not been reached yet
      assert(iter < maxSteps, "the execution was halted because the maximum number of steps were exceeded")

      //compute the intersection with the x axis and the error associated with it
      val xm = a - ((ya * (b - a)) / (yb - ya))
      val ym = f(xm)
      val error = if (absError) absoluteError(xm, xp) else relativeError(xm, xp)

      //register in the table the iteration values
      table(iter, a, b, xm, ym, error)

      //if the intersection is a root
      //or if the error is less than the tolerance
      //return the mid value as a root
      if (ym == 0.0 || error < tolerance) xm
      //otherwise iterate using a new shorter interval
      else if (ya * ym < 0)
        iterate(a, xm, ya, ym, xm, iter + 1)
      else
        iterate(xm, b, ym, yb, xm, iter + 1)
    }

    //compute the first value
    val xm = a - ((ya * (b - a)) / (yb - ya))
    val ym = f(xm)

    //register in the table the first iteration of the method
    table(0, a, b, xm, ym, "-")

    //check if the first value is a root, if yes return it
    if (ym == 0.0) xm
    //else, iterate until a root is found or until the maximum number of iterations is reached
    else if (ya * ym < 0)
      iterate(a, xm, ya, ym, xm, 1)
    else
      iterate(xm, b, ym, yb, xm, 1)
  }
}
