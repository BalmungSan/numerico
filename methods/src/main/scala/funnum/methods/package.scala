package funnum

//make the implicit value scala.language.implicitConversions visible
//to enable the implicit conversions in this module
import scala.language.implicitConversions
import scala.math
import org.scalajs.jquery.JQuery

/** Helper functions for all methods */
package object methods {
  /** Type aliases for matrices */
  type Matrix = Array[Array[Double]]
  type Vect = Array[Double]

  /** Returns the absolute error between an actual and a previous approximations */
  private[methods] def absoluteError(xa: Double, xp: Double): Double = {
    math.abs(xa - xp)
  }

  /** Returns the relative error between an actual and a previous approximations */
  private[methods] def relativeError(xa: Double, xp: Double): Double = {
    math.abs((xa - xp) / xa)
  }

  /** Returns the norm of a vector */
  private[methods] def norm(v: Vect): Double = {
    math.abs(v.map(x => math.pow(x, 2)).sum)
  }

  /** Implicit conversion from a ResultsTable to an HTML5 table */
  implicit def resultsTable2JQuery(table: ResultsTable): JQuery = table.toHTML5
}
