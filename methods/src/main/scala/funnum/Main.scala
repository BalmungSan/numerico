package funnum

//scala.js imports
import org.scalajs.jquery.jQuery //jquery
import funnum.jsapis.functionplot.{FunctionPlot, PlotConfig} //function-plot
import funnum.jsapis.mathjs.{Mathjs, MathjsImplicits} //math.js
                                                      //import the implicit conversion from mathjs CompiledFunction to Function
import MathjsImplicits._
//import all implemented numerical methods
import funnum.methods._

/** Application entry point */
object Main {
  /** Alias for Double to Double Functions */
  type Fun = Double => Double

  //Function to be called when the "Draw" button is pressed
  //updates the plot with the functions typed by the user
  private def draw(): Unit = {
    val config = PlotConfig("#plotContainer", Seq(
      ("f(x)", jQuery("#pf").value().asInstanceOf[String]),
      ("g(x)", jQuery("#pg").value().asInstanceOf[String]),
      ("h(x)", jQuery("#ph").value().asInstanceOf[String]),
      ("m(x)", jQuery("#pm").value().asInstanceOf[String])
    ))
    FunctionPlot(config)
  }

  //Function to be called when the 'Incremental Search' "Compute" button is pressed
  //updates the 'Incremental Search' results
  private def incrementalSearch(): Unit ={
    //clear the old results
    val ouputContainer = jQuery("#isOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#isTable")
    tableContainer.empty()

    //parse the function
    val f: Fun = Mathjs.compile(jQuery("#isFun").value().asInstanceOf[String])

    //get the method parameters
    val initial: Double = jQuery("#isInitial").value().asInstanceOf[String].toDouble
    val step: Double = jQuery("#isStep").value().asInstanceOf[String].toDouble
    val maxIter: Int = jQuery("#isMaxIter").value().asInstanceOf[String].toInt

    try {
      //try to execute the method
      val interval = IncrementalSearch(f)(initial, step, maxIter)
      //if the method execution succeed, update the results
      ouputContainer.append(jQuery(s"<p>The computed interval is: $interval</p>"))
    } catch {
      //if the method execution failed show a message explaining the reason
      case illArgEx: IllegalArgumentException =>  {
        val error = illArgEx.getMessage()
        ouputContainer.append(jQuery(s"<p>Bad call of the method: $error</p>"))
      }
      case assertError: AssertionError => {
        val error = assertError.getMessage()
        ouputContainer.append(jQuery(s"<p>Execution Error: $error</p>"))
      }
    } finally {
      //finally update the table
      tableContainer.append(IncrementalSearch.table)
    }
  }

  //Function to be called when the 'Bisection' "Compute" button is pressed
  //updates the 'Bisection' results
  private def bisection(): Unit ={
    //clear the old results
    val ouputContainer = jQuery("#bisOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#bisTable")
    tableContainer.empty()

    //parse the function
    val f: Fun = Mathjs.compile(jQuery("#f").value().asInstanceOf[String])

    //get the method parameters
    val lowerBound: Double = jQuery("#bisLower").value().asInstanceOf[String].toDouble
    val upperBound: Double = jQuery("#bisUpper").value().asInstanceOf[String].toDouble
    val tolerance: Double = jQuery("#bisTol").value().asInstanceOf[String].toDouble
    val maxIter: Int = jQuery("#bisMaxIter").value().asInstanceOf[String].toInt
    val error: Boolean =
      if (jQuery("input[name='bisError']:checked").value().asInstanceOf[String] == "absolute")
        true
      else
        false

    try {
      //try to execute the method
      val root = Bisection(f)((lowerBound, upperBound), tolerance, maxIter, error)
      //if the method execution succeed, update the results
      ouputContainer.append(jQuery(s"<p>The computed root is: $root</p>"))
    } catch {
      //if the method execution failed show a message explaining the reason
      case illArgEx: IllegalArgumentException =>  {
        val error = illArgEx.getMessage()
        ouputContainer.append(jQuery(s"<p>Bad call of the method: $error</p>"))
      }
      case assertError: AssertionError => {
        val error = assertError.getMessage()
        ouputContainer.append(jQuery(s"<p>Execution Error: $error</p>"))
      }
    } finally {
      //finally update the table
      tableContainer.append(Bisection.table)
    }
  }

  //Function to be called when the 'False Position' "Compute" button is pressed
  //updates the 'False Position' results
  private def falsePosition(): Unit ={
    //clear the old results
    val ouputContainer = jQuery("#fPosOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#fPosTable")
    tableContainer.empty()

    //parse the function
    val f: Fun = Mathjs.compile(jQuery("#f").value().asInstanceOf[String])

    //get the method parameters
    val lowerBound: Double = jQuery("#fPosLower").value().asInstanceOf[String].toDouble
    val upperBound: Double = jQuery("#fPosUpper").value().asInstanceOf[String].toDouble
    val tolerance: Double = jQuery("#fPosTol").value().asInstanceOf[String].toDouble
    val maxIter: Int = jQuery("#fPosMaxIter").value().asInstanceOf[String].toInt
    val error: Boolean =
      if (jQuery("input[name='fPosError']:checked").value().asInstanceOf[String] == "absolute")
        true
      else
        false

    try {
      //try to execute the method
      val root = FalsePosition(f)((lowerBound, upperBound), tolerance, maxIter, error)
      //if the method execution succeed, update the results
      ouputContainer.append(jQuery(s"<p>The computed root is: $root</p>"))
    } catch {
      //if the method execution failed show a message explaining the reason
      case illArgEx: IllegalArgumentException =>  {
        val error = illArgEx.getMessage()
        ouputContainer.append(jQuery(s"<p>Bad call of the method: $error</p>"))
      }
      case assertError: AssertionError => {
        val error = assertError.getMessage()
        ouputContainer.append(jQuery(s"<p>Execution Error: $error</p>"))
      }
    } finally {
      //finally update the table
      tableContainer.append(FalsePosition.table)
    }
  }


  //Function to be called when the 'Fixed Point' "Compute" button is pressed
  //updates the 'Fixed Points' results
  private def fixedPoint(): Unit ={
    //clear the old results
    val ouputContainer = jQuery("#fixPointOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#fixPointTable")
    tableContainer.empty()

    //parse the functions
    val f: Fun = Mathjs.compile(jQuery("#f").value().asInstanceOf[String])
    val g: Fun = Mathjs.compile(jQuery("#g").value().asInstanceOf[String])

    //get the method parameters
    val initial: Double = jQuery("#fixPointInitial").value().asInstanceOf[String].toDouble
    val tolerance: Double = jQuery("#fixPointTol").value().asInstanceOf[String].toDouble
    val maxIter: Int = jQuery("#fixPointMaxIter").value().asInstanceOf[String].toInt
    val error: Boolean =
      if (jQuery("input[name='fixPointError']:checked").value().asInstanceOf[String] == "absolute")
        true
      else
        false

    try {
      //try to execute the method
      val root = FixedPoint(f)(g)(initial, tolerance, maxIter, error)
      //if the method execution succeed, update the results
      ouputContainer.append(jQuery(s"<p>The computed root is: $root</p>"))
    } catch {
      //if the method execution failed show a message explaining the reason
      case illArgEx: IllegalArgumentException =>  {
        val error = illArgEx.getMessage()
        ouputContainer.append(jQuery(s"<p>Bad call of the method: $error</p>"))
      }
      case assertError: AssertionError => {
        val error = assertError.getMessage()
        ouputContainer.append(jQuery(s"<p>Execution Error: $error</p>"))
      }
    } finally {
      //finally update the table
      tableContainer.append(FixedPoint.table)
    }
  }

  //Function to be called when the 'Newton' "Compute" button is pressed
  //updates the 'Newton' results
  private def newton(): Unit ={
    //clear the old results
    val ouputContainer = jQuery("#newtonOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#newtonTable")
    tableContainer.empty()

    //parse the functions
    val f: Fun = Mathjs.compile(jQuery("#f").value().asInstanceOf[String])
    val df: Fun = Mathjs.compile(jQuery("#df").value().asInstanceOf[String])

    //get the method parameters
    val initial: Double = jQuery("#newtonInitial").value().asInstanceOf[String].toDouble
    val tolerance: Double = jQuery("#newtonTol").value().asInstanceOf[String].toDouble
    val maxIter: Int = jQuery("#newtonMaxIter").value().asInstanceOf[String].toInt
    val error: Boolean =
      if (jQuery("input[name='newtonError']:checked").value().asInstanceOf[String] == "absolute")
        true
      else
        false

    try {
      //try to execute the method
      val root = Newton(f)(df)(initial, tolerance, maxIter, error)
      //if the method execution succeed, update the results
      ouputContainer.append(jQuery(s"<p>The computed root is: $root</p>"))
    } catch {
      //if the method execution failed show a message explaining the reason
      case illArgEx: IllegalArgumentException =>  {
        val error = illArgEx.getMessage()
        ouputContainer.append(jQuery(s"<p>Bad call of the method: $error</p>"))
      }
      case assertError: AssertionError => {
        val error = assertError.getMessage()
        ouputContainer.append(jQuery(s"<p>Execution Error: $error</p>"))
      }
    } finally {
      //finally update the table
      tableContainer.append(Newton.table)
    }
  }

  //Function to be called when the 'Secant' "Compute" button is pressed
  //updates the 'Secant' results
  private def secant(): Unit = {
    //clear the old results
    val ouputContainer = jQuery("#secantOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#secantTable")
    tableContainer.empty()

    //parse the funtions
    val f: Fun = Mathjs.compile(jQuery("#f").value().asInstanceOf[String])

    //get method parameters
    val x0: Double = jQuery("#secantX0").value().asInstanceOf[String].toDouble
    val x1: Double = jQuery("#secantX1").value().asInstanceOf[String].toDouble
    val tolerance: Double = jQuery("#secantTol").value().asInstanceOf[String].toDouble
    val maxIter: Int = jQuery("#secantMaxIter").value().asInstanceOf[String].toInt
    val error: Boolean =
      if (jQuery("input[name='secantError']:checked").value().asInstanceOf[String] == "absolute")
        true
      else
        false

    try {
      //try to execute the method
      val root = Secant(f)(x0, x1, tolerance, maxIter, error)
      //if the method execution succeed, update the results
      ouputContainer.append(jQuery(s"<p>The computed root is: $root</p>"))
    } catch {
      //if the method execution failed show a message explaining the reason
      case illArgEx: IllegalArgumentException =>  {
        val error = illArgEx.getMessage()
        ouputContainer.append(jQuery(s"<p>Bad call of the method: $error</p>"))
      }
      case assertError: AssertionError => {
        val error = assertError.getMessage()
        ouputContainer.append(jQuery(s"<p>Execution Error: $error</p>"))
      }
    } finally {
      //finally update the table
      tableContainer.append(Secant.table)
    }
  }

  //Function to be called when the 'Multiple Root' "Compute" button is pressed
  //updates the 'Multiple Root' results
  private def multipleRoot(): Unit ={
    //clear the old results
    val ouputContainer = jQuery("#mRootOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#mRootTable")
    tableContainer.empty()

    //parse the functions
    val f: Fun = Mathjs.compile(jQuery("#f").value().asInstanceOf[String])
    val df: Fun = Mathjs.compile(jQuery("#df").value().asInstanceOf[String])
    val ddf: Fun = Mathjs.compile(jQuery("#ddf").value().asInstanceOf[String])

    //get the method parameters
    val initial: Double = jQuery("#mRootInitial").value().asInstanceOf[String].toDouble
    val tolerance: Double = jQuery("#mRootTol").value().asInstanceOf[String].toDouble
    val maxIter: Int = jQuery("#mRootMaxIter").value().asInstanceOf[String].toInt
    val error: Boolean =
      if (jQuery("input[name='mRootError']:checked").value().asInstanceOf[String] == "absolute")
        true
      else
        false

    try {
      //try to execute the method
      val root = MultipleRoot(f)(df)(ddf)(initial, tolerance, maxIter, error)
      //if the method execution succeed, update the results
      ouputContainer.append(jQuery(s"<p>The computed root is: $root</p>"))
    } catch {
      //if the method execution failed show a message explaining the reason
      case illArgEx: IllegalArgumentException =>  {
        val error = illArgEx.getMessage()
        ouputContainer.append(jQuery(s"<p>Bad call of the method: $error</p>"))
      }
      case assertError: AssertionError => {
        val error = assertError.getMessage()
        ouputContainer.append(jQuery(s"<p>Execution Error: $error</p>"))
      }
    } finally {
      //finally update the table
      tableContainer.append(MultipleRoot.table)
    }
  }

  //Function to be called when the 'Gauss Elimination' "Compute" button is pressed
  //updates the 'Gauss Elimination' results
  private def gauss_elimitacion(): Unit = {
    //clear the old results
    val ouputContainer = jQuery("#gaussOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#gaussTable")
    tableContainer.empty()

    //get method parameters
    val n: Int = jQuery("td","#indepen_terms").length

    val m: Matrix = for {
      i <- (1 to n).toArray
      row: Vect = for {
        j <- (1 to n).toArray
      } yield jQuery(s"#matrix_${i}_${j}").value().asInstanceOf[String].toDouble
      bi =  jQuery(s"#term${i}").value().asInstanceOf[String].toDouble
    } yield row :+ bi
    //println(m.map(_.mkString(", ")).mkString("\n"))

    val result = GaussElimination(m)
    ouputContainer.append(jQuery(s"<table class='table table-striped'><thead><tr>"))
    for {
      i <- 1 to n
    } yield ouputContainer.append(jQuery(s"<th style='text-align: center;'>x<sub>${i}</sub></th>"))
    ouputContainer.append(jQuery(s"</tr></thead><tbody><tr>"))
    for {
      i <- result
    } yield ouputContainer.append(jQuery(s"<td style='text-align: center;'>${i}</td>"))
    ouputContainer.append(jQuery(s"</tr></tbody><table>"))

  }

  private def ludescomposition(): Unit = {
    val ouputContainer = jQuery("#ludOutput")
    ouputContainer.empty()
    //get method parameters

    val methodString = jQuery("input[name='ludmethod']:checked").value().asInstanceOf[String]
    val method = methodString match {
      case "cholesky" => Cholesky
      case "doolittle" => Doolittle
      case "gauss" => GaussFactorization
      case "crout" => Crout
    }

    //println(method)
    val n: Int = jQuery("td","#indepen_terms").length
    val m: Matrix = for {
      i <- (1 to n).toArray
      row: Vect = for {
        j <- (1 to n).toArray
      } yield jQuery(s"#matrix_${i}_${j}").value().asInstanceOf[String].toDouble
    } yield row

    val b: Vect = for {
      i <- (1 to n).toArray
    } yield jQuery(s"#term${i}").value().asInstanceOf[String].toDouble

    val solve = LUDecomposition(method)(m,Seq(b))
    var result = solve(0)
    ouputContainer.append(jQuery(s"<table class='table table-striped col-md-12'><thead><tr>"))
    for {
      i <- 1 to n
    } yield ouputContainer.append(jQuery(s"<th style='text-align: center;'>x<sub>${i}</sub></th>"))
    ouputContainer.append(jQuery(s"</tr></thead><tbody><tr>"))
    for {
      i <- result
    } yield ouputContainer.append(jQuery(s"<td style='text-align: center;'>${i}</td>"))
    ouputContainer.append(jQuery(s"</tr></tbody><table>"))

  }

  private def jacobi(): Unit = {
    val ouputContainer = jQuery("#jacobiOutput")
    ouputContainer.empty()
    val tableContainer = jQuery("#jacobiTable")
    tableContainer.empty()
    val n: Int = jQuery("td","#indepen_terms").length
    val m: Matrix = for {
      i <- (1 to n).toArray
      row: Vect = for {
        j <- (1 to n).toArray
      } yield jQuery(s"#matrix_${i}_${j}").value().asInstanceOf[String].toDouble
    } yield row

    val b: Vect = for {
      i <- (1 to n).toArray
    } yield jQuery(s"#term${i}").value().asInstanceOf[String].toDouble

    val initial: Vect = for {
      i <- (1 to n).toArray
    } yield jQuery(s"#jacobiterm${i}").value().asInstanceOf[String].toDouble
    println(m,b,initial)
    val tolerance: Double = jQuery("#jacobiPointTol").value().asInstanceOf[String].toDouble
    val maxIter: Int = jQuery("#jacobiPointMaxIter").value().asInstanceOf[String].toInt
    val relajation: Double = jQuery("#jacobiPointRelajation").value().asInstanceOf[String].toDouble
    val error: Boolean =
      if (jQuery("input[name='jacobiError']:checked").value().asInstanceOf[String] == "absolute")
        true
      else
        false
    try {
      //try to execute the method
      val x = Jacobi(m,initial,b,relajation)(maxIter,tolerance, error)
    } catch {
      //if the method execution failed show a message explaining the reason
      case illArgEx: IllegalArgumentException =>  {
        val error = illArgEx.getMessage()
        ouputContainer.append(jQuery(s"<p>Bad call of the method: $error</p>"))
      }
      case assertError: AssertionError => {
        val error = assertError.getMessage()
        ouputContainer.append(jQuery(s"<p>Execution Error: $error</p>"))
      }
    } finally {
      //finally update the table
      tableContainer.append(Jacobi.table)
    }

  }

  /** Main Method */
  def main(args: Array[String]): Unit = {
    //Add the onclick action to the buttons
    jQuery("#plotButton").click(() => draw())
    jQuery("#isButton").click(() => incrementalSearch())
    jQuery("#bisButton").click(() => bisection())
    jQuery("#fPosButton").click(() => falsePosition())
    jQuery("#fixPointButton").click(() => fixedPoint())
    jQuery("#newtonButton").click(() => newton())
    jQuery("#secantButton").click(() => secant())
    jQuery("#mRootButton").click(() => multipleRoot())
    jQuery("#gaussButton").click(() => gauss_elimitacion())
    jQuery("#ludButton").click(() => ludescomposition())
    jQuery("#jacobiButton").click(() => jacobi())
  }
}
