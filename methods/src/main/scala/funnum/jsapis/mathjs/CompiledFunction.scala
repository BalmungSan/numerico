package funnum.jsapis.mathjs

//import scalajs.js package for interoperability with JavaScript libraries
import scalajs.js

/** Scala.js object for encapsulating the result of a compiled expression by math.js */
abstract class CompiledFunction extends js.Object {
  /** Evaluate the expression with a given scope */
  def eval(scope: js.Object): Double
}
