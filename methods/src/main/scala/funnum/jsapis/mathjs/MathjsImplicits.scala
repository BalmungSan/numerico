package funnum.jsapis.mathjs

//make the implicit value scala.language.implicitConversions visible
//to enable the implicit conversions in this module
import scala.language.implicitConversions

/** Implicit conversions for the Mathjs facade */
object MathjsImplicits {
  /** Implicit Conversion from CompiledFunction to Double => Double Function */
  implicit def compiledFunction2Function1(cf: CompiledFunction): Double => Double = {
    (_x: Double) => {
      val scope = new scalajs.js.Object {
        val x = _x
      }
      cf.eval(scope)
    }
  }
}
