package funnum.jsapis.mathjs

import scalajs.js
import js.annotation.JSGlobal

/** Math.js scalajs facade
  * http://mathjs.org/
  */
@js.native
@JSGlobal("math")
object Mathjs extends js.Object {
  @js.native
  def compile(expr: String): CompiledFunction = js.native
}
