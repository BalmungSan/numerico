package funnum.jsapis.functionplot

import scalajs.js
import js.annotation.JSGlobal

/** functionPlot constructor */
@js.native
@JSGlobal("functionPlot")
class FunctionPlot(chartConfig: PlotConfig) extends js.Object

/** Alias for calling FunctionPlot without using the new keyword */
object FunctionPlot {
  def apply(chartConfig: PlotConfig): FunctionPlot = new FunctionPlot(chartConfig)
}
