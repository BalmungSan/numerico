package funnum.jsapis;

//import scalajs.js package for interoperability with JavaScript libraries
import scalajs.js
import js.JSConverters._

/** Scala.js facade for the js library function-plot
  *  https://github.com/mauriciopoppe/function-plot
  */
package object functionplot {
  /** Plot Data Config */
  private[functionplot] class PlotDataConfig extends js.Object {
    val title: js.UndefOr[String] = js.undefined
    val fn: js.UndefOr[String] = js.undefined
    val sampler: js.UndefOr[String] = "builtIn"
    val fnType: js.UndefOr[String] = "linear"
    val graphType: js.UndefOr[String] = "polyline"
  }

  /** Factory of PlotDataConfig */
  private[functionplot] object PlotDataConfig {
    /** Creates a new DataConfig for a given function */
    def apply(name: String, fun: String): PlotDataConfig = {
      new PlotDataConfig {
        override val title: js.UndefOr[String] = name
        override val fn: js.UndefOr[String] = fun
      }
    }
  }

  /** Plot Config */
  class PlotConfig extends js.Object {
    val title: js.UndefOr[String] = "Plotter"
    val target: js.UndefOr[String] = js.undefined
    val data: js.UndefOr[js.Array[PlotDataConfig]] = js.undefined
    val disableZoom: js.UndefOr[Boolean] = false
    val grid: js.UndefOr[Boolean] = true
    val plugins = Seq(new ZoomBoxPlugin()).toJSArray
  }

  /** Factory of PlotConfig */
  object PlotConfig {
    /** Creates a new PlotConfig */
    def apply(target: String, functions: Seq[(String,String)]): PlotConfig = {
      val _target = target
      val _functions = functions map { case(name, fun) => PlotDataConfig(name, fun) }
      new PlotConfig {
        override val target: js.UndefOr[String] = _target
        override val data: js.UndefOr[js.Array[PlotDataConfig]] = _functions.toJSArray
      }
    }
  }
}
