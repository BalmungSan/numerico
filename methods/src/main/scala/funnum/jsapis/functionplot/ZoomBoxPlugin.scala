package funnum.jsapis.functionplot

import scalajs.js
import js.annotation.JSGlobal

/** FunctionPlot ZoomBox Plugin*/
@js.native
@JSGlobal("functionPlot.plugins.zoomBox")
private[functionplot] class ZoomBoxPlugin extends js.Object
