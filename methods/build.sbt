//project settings
val appName = "Functional Numerical Methods"
name := appName
version := "2.0.0"
scalaVersion := "2.12.4"
scalacOptions ++= Seq("-deprecation", "-feature")
scalacOptions in (doc) ++= Seq("-groups", "-implicits")
scalacOptions in (Compile, doc) ++= Seq("-doc-title", appName)

//scalajs settings
enablePlugins(ScalaJSPlugin)
scalaJSUseMainModuleInitializer := true
scalacOptions += "-P:scalajs:sjsDefinedByDefault"

//dependencies
libraryDependencies += "be.doeraene" %%% "scalajs-jquery" % "0.9.1" //jQuery

//JS dependencies
jsDependencies += "org.webjars" % "jquery" % "2.1.4" / "2.1.4/jquery.js" minified "2.1.4/jquery.min.js"
jsDependencies += "org.webjars" % "mathjs" % "3.2.1" / "3.2.1/math.js" minified "3.2.1/math.min.js"
jsDependencies += "org.webjars" % "d3js" % "3.5.17" / "3.5.17/d3.js" minified "3.5.17/d3.min.js"
jsDependencies += "org.webjars.bower" % "function-plot" % "1.17.0" / "1.17.0/dist/function-plot.js" dependsOn "d3.js" dependsOn "math.js"
