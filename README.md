# Numérico

Tareas de Ánalisis Numérico

* **epsilon:** programas para calcular el epsilon de la mquina usando precisión simple, doble y cuadruple
    - _scala_
    - _python_
    - _c++_
* **matrix:** suma y multiplicación paralelas de matrices en scala
* **parallelMatrices:** solver distribuido de grandes sistemas de ecuaciones lineales en spark
* **taylor:** implementación generica de series de taylor y algunas comunes:
    - _$`e^{x}`$ at $`a = 0`$_
    - _$`\sin{x}`$ at $`a = 0`$_
    - _$`\cos{x}`$ at $`a = 0`$_
    - _$`\ln{1 + x}`$ at $`a = 0`$_
    - _$`\tan^{-1}{x}`$ at $`a = 0`$_
* **methods:** diferentes metodos númericos implementados en scala.js
    - _Busqueda Incremental_
    - _Bisección_
    - _Regla Falsa_
    - _Punto Fijo_
    - _Newton_
    - _Secante_
    - _Raíces Múltiples_
