package taylor

import scala.annotation.tailrec
import scala.math.{pow, Fractional}

class Taylor[X, Y](val sigma: (X, Long) => Y)(implicit numX: Fractional[X], numY: Fractional[Y]) {
  import Fractional.Implicits._

  def apply(x: X, n: Long): Y = {
    val serie = for {
      i <- (0L to n).par
    } yield sigma(x, i)

    serie.sum
  }

  def +(that: Taylor[X, Y]): Taylor[X, Y] = {
    val newSigma = (x: X, i: Long) => this.sigma(x, i) + that.sigma(x, i)
    new Taylor(newSigma)
  }

  def -(that: Taylor[X, Y]): Taylor[X, Y] = {
    val newSigma = (x: X, i: Long) => this.sigma(x, i) - that.sigma(x, i)
    new Taylor(newSigma)
  }

  def *(that: Taylor[X, Y]): Taylor[X, Y] = {
    val newSigma = (x: X, i: Long) => this.sigma(x, i) * that.sigma(x, i)
    new Taylor(newSigma)
  }

  def /(that: Taylor[X, Y]): Taylor[X, Y] = {
    val newSigma = (x: X, i: Long) => this.sigma(x, i) / that.sigma(x, i)
    new Taylor(newSigma)
  }
}

object Taylor {
  //factorial
  private def factorial(n: Long): Long = {
    require(n >= 0, "factorial of negative number")

    @tailrec def factAcc(acc: Long, n: Long): Long = {
      if (n == 0) acc
      else if (n == 1) acc
      else factAcc(n * acc, n-1)
    }

    factAcc(1L, n)
  }

  //functions
  //e^x at a = 0
  private val eulerX_a0 = (x: Double, i: Long) => {
    pow(x, i) / factorial(i)
  }
  lazy val eX = new Taylor(eulerX_a0)

  //sin(x) at a = 0
  private val sinX_a0 = (x: Double, i: Long) => {
    val a = (2 * i) + 1 //2n + 1
    (pow(-1, i) * pow(x, a)) / factorial(a)
  }
  lazy val sinX = new Taylor(sinX_a0)

  //cos(x) at a = 0
  private val cosX_a0 = (x: Double, i: Long) => {
    val a = 2 * i //2k
    (pow(-1, i) * pow(x, a)) / factorial(a)
  }
  lazy val cosX = new Taylor(cosX_a0)

  //ln(1 + x) at a = 0
  private val lnX1_a0 = (x: Double, i: Long) => {
    val a = i + 1 //n + 1
    (pow(-1, i) * pow(x, a)) / a
  }
  lazy val lnX1 = new Taylor(lnX1_a0)

  //tan⁻1(x) at a = 0
  private val tan1X_a0 = (x: Double, i: Long) => {
    val a = (2 * i) + 1//2n + 1
    (pow(-1, i) * pow(x, a)) / a
  }
  lazy val tan1X = new Taylor(tan1X_a0)

  /** */
  def apply[X: Fractional, Y: Fractional](sigma: (X, Long) => Y) = new Taylor(sigma)

  def apply[X: Fractional](constant: X) = new Taylor((_: X, _: Long) => constant)
}
