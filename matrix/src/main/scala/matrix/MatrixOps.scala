package matrix

//imports
import scala.collection.GenSeq
import scala.collection.parallel.ParSeq

object MatrixOps {
  /** Returns the sum of two numerical matrices
   *
   * @tparam T any numeric class
   * @param m1 a matrix of size n*m
   * @param m2 a matrix of size n*m
   *
   * @return a matrix of the same size as the input matrices
   *
   * @note This function is parallel by the rows
   * @throws java.lang.IllegalArgumentException if both matrices are not of the same size
   *
   * @see Numeric
   * @see GenSeq
   * @see ParSeq
   */
  def sumMatrices[T: Numeric](m1: GenSeq[GenSeq[T]], m2: GenSeq[GenSeq[T]]): ParSeq[GenSeq[T]] = {
    //import the Numeric + function
    import Numeric.Implicits._

    //check assumptions
    require(m1.length == m2.length, "Both matrices must have the same number of rows")
    val m1RowLength = m1.head.length
    require(m1 forall { x => x.length == m1RowLength }, "All rows in m1 must have the same length")
    val m2RowLength = m2.head.length
    require(m2 forall { x => x.length == m2RowLength }, "All rows in m2 must have the same length")
    require(m1RowLength == m2RowLength, "Both matrices must have the same number of columns")

    for {
      (rowM1, rowM2) <- (m1 zip m2).par
    } yield (rowM1 zip rowM2) map { case(c1, c2) => c1 + c2 }
  }

  /** Returns the multiplcation of two numerical matrices
   *
   * @tparam T any numeric class
   * @param m1 a matrix of size n*m
   * @param m2 a matrix of size m*p
   *
   * @return a matrix of size n*p
   *
   * @note This function is parallel by the rows of m1 and the columns of m2
   * @throws java.lang.IllegalArgumentException if the number of rows in m2 is not equal to the number of columns in m1
   *
   * @see Numeric
   * @see GenSeq
   * @see ParSeq
   */
  def multMatrices[T: Numeric](m1: GenSeq[GenSeq[T]], m2: GenSeq[GenSeq[T]]): ParSeq[ParSeq[T]] = {
    //import the Numeric + function
    import Numeric.Implicits._

    //check assumptions
    val m1RowLength = m1.head.length
    require(m1 forall { x => x.length == m1RowLength }, "All rows in m1 must have the same length")
    val m2RowLength = m2.head.length
    require(m2 forall { x => x.length == m2RowLength }, "All rows in m2 must have the same length")
    require(m1RowLength == m2.length, "The number of rows in m2 must be equal to the number of columns in m1")

    val m2T = m2.par.transpose
    for {
      row <- m1.par
    } yield for {
      col <- m2T
    } yield ((row zip col) map { case(v1, v2) => v1 * v2 }).sum
  }
}
