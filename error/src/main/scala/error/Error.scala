package error

import scala.math.BigDecimal

sealed abstract class Error(val value: BigDecimal, val error: BigDecimal, val rError: BigDecimal) {
  def + (that: Error): Error
  def - (that: Error): Error
  def * (that: Error): Error
  def / (that: Error): Error
}

case class AbsoluteError(
  override val value: BigDecimal,
  override val error: BigDecimal,
  override val rError: BigDecimal)
    extends Error(value, error, rError) {
  override def + (that: Error): Error = {
    val newValue = this.value + that.value
    val newError = this.error + that.error + rError
    new AbsoluteError(newValue, newError, rError)
  }

  override def - (that: Error): Error = {
    val newValue = this.value - that.value
    val newError = this.error - that.error + rError
    new AbsoluteError(newValue, newError, rError)
  }

  override def * (that: Error): Error = {
    val newValue = this.value * that.value
    val newError = (that.value * this.error) + (this.value * that.error) + rError
    new AbsoluteError(newValue, newError, rError)
  }

  override def / (that: Error): Error = {
    val newValue = this.value / that.value
    val newError = ((1 / that.value) * this.error) - ((this.value / that.value.pow(2)) * that.error) + rError
    new AbsoluteError(newValue, newError, rError)
  }
}

object AbsoluteError {
  def apply(value: BigDecimal, error: BigDecimal, rError: BigDecimal) = {
    new AbsoluteError(value, error, rError)
  }
}

case class RelativeError(
  override val value: BigDecimal,
  override val error: BigDecimal,
  override val rError: BigDecimal)
    extends Error(value, error, rError) {
  override def + (that: Error): Error = {
    val newValue = this.value + that.value
    val xy = this.value + that.value
    val newError = ((this.value / xy) * this.error) + ((that.value / xy) * that.error) + rError
    new RelativeError(newValue, newError, rError)
  }

  override def - (that: Error): Error = {
    val newValue = this.value - that.value
    val xy = this.value - that.value
    val newError = ((this.value / xy) * this.error) - ((that.value / xy) * that.error)
    new RelativeError(newValue, newError, rError)
  }

  override def * (that: Error): Error = {
    val newValue = this.value * that.value
    val newError = this.error + that.error + rError
    new RelativeError(newValue, newError, rError)
  }

  override def / (that: Error): Error = {
    val newValue = this.value / that.value
    val newError = this.error - that.error + rError
    new RelativeError(newValue, newError, rError)
  }
}

object RelativeError {
  def apply(value: BigDecimal, error: BigDecimal, rError: BigDecimal) = {
    new RelativeError(value, error, rError)
  }
}
