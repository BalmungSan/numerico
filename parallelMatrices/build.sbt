val appName = "Functional Parallel Numerical"
name         := appName
version      := "1.3.2"

scalaVersion := "2.11.11"
scalacOptions ++= Seq("-deprecation", "-feature")
scalacOptions in (doc) ++= Seq("-groups", "-implicits")
scalacOptions in (Compile, doc) ++= Seq("-doc-title", appName)

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.1" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.1.1" % "provided"
