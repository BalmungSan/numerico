package funnum.parallel.methods

//import the parallel matrix package
import funnum.parallel.matrix._
//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec
//import the BigDecimal class for using 128-bit floating point numbers
import scala.math.BigDecimal
//import SparkSession to allow the use of the implicit encoders for case classes
import org.apache.spark.sql.SparkSession

/**
  * Parallel method for jacobi
  */
object Jacobi {
  //constants
  private val one  = BigDecimal(1.0d)
  private val zero = BigDecimal(0.0d)

  /**
    * @param a matrix to apply the methods
    * @param _initial vector of the initial aproximations ,
    * @param r relaxation , acelerates the method convergence default value 1
    */
  def apply(a: Matrix, _initial: Matrix, _b: Matrix, _r: BigDecimal = one)
    (maxIter: Int, tol: BigDecimal, absError: Boolean = true)
    (implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._
    val sc = spark.sparkContext
    val numpar = sc.defaultParallelism

    //check preconditions
    require(_r > zero, "the relaxation value must be greater than zero")
    require(tol > zero, "the tolerance must be greater than zero")
    require(maxIter > 0, "the maximum number of iterations must be greater than zero")

    //method preparations
    val n   = sc.broadcast(_b.head.j)
    val r   = sc.broadcast(_r)
    val r1  = sc.broadcast(one - _r)
    val _d  = a filter { $"i" === $"j" } map { case MatrixElement(i, j, v) => MatrixElement(i, n.value, v) }
    val _aD = a map {
      x => x match {
        case MatrixElement(i, j, v) => if (i == j) MatrixElement(i, j, zero) else x
      }
    }

    //cache reusable collections
    val b  = _b.repartition(numpar, $"j").rdd.setName("b").localCheckpoint().toDS
    val d  = _d.repartition(numpar, $"j").rdd.setName("d").localCheckpoint().toDS
    val aD = _aD.repartition(numpar, $"j").rdd.setName("aD").localCheckpoint().toDS
    val initial = _initial.repartition(numpar, $"j").rdd.setName("x0").localCheckpoint().toDS

    //functional implementation of an iterative process using a tail recursive function
    @tailrec
    def iterate(xp: Matrix, xpNorm: BigDecimal, iter: Int): Matrix = {
      //compute the new solution
      val act = (b - (aD * xp)) / d
      val _xa = if (_r != one) {
        act.joinWith(xp, act.col("i") === xp.col("i"), "inner") map {
          case (MatrixElement(i, j, va), MatrixElement(_, _, vp)) => MatrixElement(i, j, (r.value * va) + (r1.value * vp))
        }
      } else {
        act
      }

      //cache actual solution to improve performance
      val xa = _xa.repartition(numpar, $"j").rdd.setName(s"x$iter").localCheckpoint().toDS

      //compute the norm of the actual solution
      val xaNorm = norm(xa)

      //free previous solution, because is not needed anymore
      xp.unpersist(false)

      //compute the error of the actual solution
      val error = if (absError) absoluteError(xaNorm, xpNorm) else relativeError(xaNorm, xpNorm)

      //check if the error associated with the actual solution is less than the tolerance
      //if yes, return the actual solution
      //otherwise, iterate
      if (error < tol || iter == maxIter) xa
      else iterate(xa, xaNorm, iter + 1)
    }

    //iterate until a solution is found or the maximum number of iterations is reached
    val solution = iterate(initial, norm(initial), 1)

    //sort the computed solution
    val result = solution.sort($"i").repartition(numpar).rdd.setName("xa").localCheckpoint().toDS

    //free unnecessary intermediate collections
    d.unpersist(false)
    aD.unpersist(false)
    initial.unpersist(false)

    //return the final result
    result
  }

  /** Returns the norm of a vector */
  /**
    * Returns the norm of a vector
    * @param m Matrix, but is realli a matrix of only one dimension (vector) to apply the method
    * @param spark
    * @return the norm, in this que the sum of the absolutes values
    */
  private def norm(m: Matrix) (implicit spark: SparkSession): BigDecimal = {
    //import the spark implicits
    import spark.implicits._
    //import the dataset sql function
    import org.apache.spark.sql.{functions => sqlfun}
    m.agg(sqlfun.sum(sqlfun.abs($"v"))).head.getDecimal(0)
  }

  /** Returns the absolute error between an actual and a previous approximations */
  /** Returns the absolute error between an actual and a previous approximations
    *
    * @param a first value
    * @param b second value
    * @return the absolute Error beetween a and b
    */
  private def absoluteError(a: BigDecimal, b: BigDecimal): BigDecimal = {
    (a - b).abs
  }

  /** Returns the relative error between an actual and a previous approximations */
  /**
    *Returns the relative error between an actual and a previous approximations
    * @param a first value
    * @param b second value
    * @return the relative error beetween a and b
    *
    */
  private def relativeError(a: BigDecimal, b: BigDecimal): BigDecimal = {
    ((a - b)/a).abs
  }
}
