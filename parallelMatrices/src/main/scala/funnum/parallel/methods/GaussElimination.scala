package funnum.parallel.methods

//import the parallel matrix package
import funnum.parallel.matrix._
//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec
//import SparkSession to allow the use of the implicit encoders for case classes and the sql functions
import org.apache.spark.sql.{Column, SparkSession, functions => sqlfun}

/**
  *
  */
object GaussElimination {
  /**
    * Method that solve a system of linear Equation of size N*N in parallel
    * @param a matrix that is goinf to be apply the method but in parallel
    * @param b matrix of only one dimension similar to a vector
    * @return result , that is the computed solution
    */
  def apply(a: Matrix, b: Matrix)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._
    val sc = spark.sparkContext
    val numpar = sc.defaultParallelism

    //get the iteration limit
    val _n = b.head.j
    val n  = sqlfun.lit(_n)
    val end = _n - 2L

    //create the augmented matrix ab by adding b as the last column of a
    val ab = (a union b).repartition(numpar, $"j").rdd.setName("ab").localCheckpoint().toDS

    //mutable Array to hold the order of the equations after applying pivoting
    val _xOrder = (0L to (_n - 1L)).toArray

    //functional implementation of the row reduction iterative process using a tail recursive function
    @tailrec
    def eliminate(_ab: Matrix, _k: Long): Matrix = {
      //create a literal column with the value of the actual iteration
      val k = sqlfun.lit(_k)

      //apply total pivoting to the matrix
      //get the maximum element in a
      val _max = _ab filter { $"i" >= k && $"j" >= k && $"j" < n } agg { sqlfun.max(sqlfun.abs($"v")) }
      val max  = sqlfun.lit(_max.head.getDecimal(0))

      //get the i and j indexes of the maximum value
      val _argmax = _ab filter { $"v" === max }
      val argmax  = _argmax.head
      val iMax = sc.broadcast(argmax.i)
      val jMax = sc.broadcast(argmax.j)

      //execute the row and column pivoting
      val __ab = _ab map {
        case MatrixElement(i, j, v) if (i == iMax.value && j == jMax.value) => MatrixElement(_k, _k, v)
        case MatrixElement(i, j, v) if (i == iMax.value && j == _k) => MatrixElement(_k, jMax.value, v)
        case MatrixElement(i, j, v) if (i == _k && j == jMax.value) => MatrixElement(iMax.value, _k, v)
        case MatrixElement(i, j, v) if (i == _k && j == _k) => MatrixElement(iMax.value, jMax.value, v)
        case MatrixElement(i, j, v) if (i == iMax.value) => MatrixElement(_k, j, v)
        case MatrixElement(i, j, v) if (i == _k) => MatrixElement(iMax.value, j, v)
        case MatrixElement(i, j, v) if (j == jMax.value) => MatrixElement(i, _k, v)
        case MatrixElement(i, j, v) if (j == _k) => MatrixElement(i, jMax.value, v)
        case MatrixElement(i, j, v) => MatrixElement(i, j, v)
      }
      val ab = __ab.repartition(numpar, $"j").rdd.setName(s"abP${_k}").localCheckpoint().toDS

      //save the new order of the equations
      val aux = _xOrder(_k.toInt)
      _xOrder(_k.toInt) = _xOrder(iMax.value.toInt)
      _xOrder(iMax.value.toInt) = aux
      //---------------------------------------------------------------------------

      //get the pivot row and the a[k][k] element
      val _pivot = ab filter { $"i" === k  && $"j" >= k }
      val pivot  = _pivot.repartition(numpar, $"j").rdd.setName(s"pivot$k").localCheckpoint().toDS
      val _akk  = pivot filter { $"j" === k }
      val akk   = sc.broadcast(_akk.head.v)

      //compute the multipliers for this iteration
      val m = ab filter { $"i" > k && $"j" === k } map {
        case MatrixElement(i, j, v) => MatrixElement(i, j, v / akk.value)
      }

      //compute a submatrix of coefficients where each element ij is composed of
      //the multiplication of the i multiplier and the j pivot
      val pm = pivot.joinWith(m, sqlfun.lit(true), "cross") map {
        case (MatrixElement(_, j, pj), MatrixElement(i, _, mi)) => MatrixElement(i, j, mi * pj)
      }

      //subtract to the augmented matrix all elements ij in the coefficient matrix
      val _abk = ab.joinWith(
        pm,
        ab.col("i") === pm.col("i") && ab.col("j") === pm.col("j"),
        "left_outer") map {
        case (abij, null) => abij
        case (MatrixElement(i, j, abij), MatrixElement(_, _, pmij)) => MatrixElement(i, j, abij - pmij)
      }

      //cache the actual reduction to improve performance
      val abk = _abk.repartition(numpar, $"j").rdd.setName(s"ab${_k}").localCheckpoint().toDS

      //if we reached the last step of reduction, return
      //else, continue with the next step
      if (_k == end) abk
      else eliminate(abk, _k + 1L)
    }

    //perform row reduction of the matrix
    val reduced = eliminate(ab, 0L)

    //functional implementation of the regressive substitution process using a tail recursive function
    /**
      * recursive regresive substitution
      * @param x Matrix to replace the values with
      * @param _k current iteration
      */
    @tailrec
    def solve(x: Matrix, _k: Long): Matrix = {
      //create a literal column with the value of the actual iteration
      val k = sqlfun.lit(_k)

      //get the a[k][k] element of the reduced matrix
      val akk = sc.broadcast(reduced.where($"i" === k && $"j" === k).head.v)

      //compute the x[k] element as the division of the accumulate value with a[k][k]
      val xk_ = x map {
        case MatrixElement(i, j, v) if (i == _k) => MatrixElement(i, j, v / akk.value)
        case others => others
      }

      //if this is the last iteration return the computed x vector
      if (_k == 0L) xk_
      //if not, parallel reduce x[k] from the matrix
      else {
        //get x[k]
        val xkk = sc.broadcast(xk_.where($"i" === k).head.v)

        //multiply the k column of the reduced matrix with x[k]
        val akx = reduced filter { $"j" === k && $"i" < k } map {
          case MatrixElement(i, j, rij) => MatrixElement(i, j, rij * xkk.value)
        }

        //subtract from the acumulator the a[k] * x[k] column
        val _xk = xk_.joinWith(
          akx,
          xk_.col("i") === akx.col("i") && xk_.col("i") < k,
          "left_outer") map {
          case (xki, null) => xki
          case (MatrixElement(i, j, xki), MatrixElement(_, _, akxi)) => MatrixElement(i, j, xki - akxi)
        }
        val xk = _xk.repartition(numpar, $"i").rdd.setName(s"x${_k}").localCheckpoint().toDS

        //iterate
        solve(xk, _k - 1L)
      }
    }

    //solve the equation system using the reduced matrix
    val xi = reduced filter { $"j" === sqlfun.lit(n) }
    val _solution = solve(xi, _n - 1L)

    //update the order of the computed x with the final equation order
    val xOrder = sc.broadcast(_xOrder)
    val solution = _solution map {
      case MatrixElement(i, j, v) => MatrixElement(xOrder.value(i.toInt), j, v)
    }

    //sort the computed solution
    val result = solution.sort($"i", $"j").repartition(numpar).rdd.setName("x").localCheckpoint().toDS

    //return the computed solution of the system
    result
  }
}
