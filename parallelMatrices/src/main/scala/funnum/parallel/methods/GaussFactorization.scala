package funnum.parallel.methods

//import the parallel matrix package
import funnum.parallel.matrix._
//import the tailrec annotation to ensure the compiler
//implements the recursive function as a loop
import scala.annotation.tailrec
//import SparkSession to allow the use of the implicit encoders for case classes
import org.apache.spark.sql.SparkSession
//import the dataset sql function
import org.apache.spark.sql.{functions => sqlfun}

/**
  *
  */
object GaussFactorization {
  //constants
  private val one  = BigDecimal(1.0d)
  private val zero = BigDecimal(0.0d)

  /**
    * Method that decompose the matrix in L and U but in parallel
    * @param a matrix that is goinf to be apply the method but in parallel
    * @param b matrix of only one dimension similar to a vector
    * @return result , that is the computed solution
    *
    */
  def apply(_a: Matrix, _b: Matrix)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._
    val sc = spark.sparkContext
    val numpar = sc.defaultParallelism

    //repartition a & b by rows
    val ai = _a.repartition(numpar, $"j").rdd.setName("a").localCheckpoint().toDS
    val b  = _b.repartition(numpar, $"i").rdd.setName("b").localCheckpoint().toDS

    //initialize l as an empty matrix
    val li = spark.emptyDataset[MatrixElement].repartition(numpar, $"j")

    //get the iteration limit
    val n = ai.agg(sqlfun.max($"j")).head.getLong(0)
    val end = n - 1L

    //functional implementation of the row reduction iterative process using a tail recursive function
    @tailrec
    def eliminate(a: Matrix, l: Matrix, _k: Long): (Matrix, Matrix) = {
      //create a literal column with the value of the actual iteration
      val k = sqlfun.lit(_k)

      //get the pivot row and the a[k][k] element
      val _pivot = a filter { $"i" === k  && $"j" >= k }
      val pivot  = _pivot.repartition(numpar, $"j").rdd.setName(s"pivot$k").localCheckpoint().toDS
      val _akk   = pivot filter { $"j" === k }
      val akk    = sc.broadcast(_akk.head.v)

      //compute the multipliers for this iteration
      val _m = a filter { $"j" === k } map {
        case MatrixElement(i, j, v) if (i > j)  => MatrixElement(i, j, v / akk.value)
        case MatrixElement(i, j, v) if (i < j)  => MatrixElement(i, j, zero)
        case MatrixElement(i, j, v) if (i == j) => MatrixElement(i, j, one)
      }
      val m = _m.repartition(numpar, $"i").rdd.setName(s"m$k").localCheckpoint().toDS

      //add a new column to l, composed of the multipliers of the actual iteration
      val lk = (l union m).rdd.setName(s"l$k").localCheckpoint().toDS

      //compute a submatrix of coefficients where each element ij is composed of
      //the multiplication of the i multiplier and the j pivot
      val _pm = pivot.joinWith(m, m.col("i") > k && pivot.col("j") >= k, "cross") map {
        case (MatrixElement(_, j, pj), MatrixElement(i, _, mi)) => MatrixElement(i, j, mi * pj)
      }
      val pm = _pm.repartition(numpar, $"j").rdd.setName(s"pm$k").localCheckpoint().toDS

      //subtract to the augmented matrix all elements ij in the coefficient matrix
      val _ak = a.joinWith(
        pm,
        a.col("i") === pm.col("i") && a.col("j") === pm.col("j"),
        "left_outer") map {
        case (aij, null) => aij
        case (MatrixElement(i, j, aij), MatrixElement(_, _, pmij)) => MatrixElement(i, j, aij - pmij)
      }

      //cache the actual reduction to improve performance
      val ak = _ak.repartition(numpar, $"j").rdd.setName(s"a${_k}").localCheckpoint().toDS

      //if we reached the last step of reduction, return
      //else, continue with the next step
      if (_k == end) (ak, lk)
      else eliminate(ak, lk, _k + 1)
    }

    //perform row reduction of the matrix
    val (reduced, _l) = eliminate(ai, li, 0L)

    //create u from the reduced matrix
    val _u = reduced map {
      case MatrixElement(i, j, v) if (i <= j) => MatrixElement(i, j, v)
      case MatrixElement(i, j, _) if (i > j)  => MatrixElement(i, j, zero)
    }
    val u = _u.repartition(numpar, $"j").rdd.setName("u").localCheckpoint().toDS

    //add the last column to l
    val llc = (sc.parallelize(0L to n) map {
      case i if (i < n)  => MatrixElement(i, n, zero)
      case i if (i == n) => MatrixElement(i, n, one)
    }).toDS()
    val l = (_l union llc).repartition(numpar, $"j").rdd.setName("l").localCheckpoint().toDS

    //functional implementation of the progressive substitution process using a tail recursive function
    /**
      *
      * @param z Triangular inferior matrix resulting from the decomposition (L)
      * @param _k iteration
      * @return b, that is the partial result of the sistem
      */
    @tailrec
    def progressiveSubstitution(z: Matrix, _k: Long): Matrix = {
      //create a literal column with the value of the actual iteration
      val k = sqlfun.lit(_k)

      //get the l[k][k] element
      val lkk = sc.broadcast(l.where($"i" === k && $"j" === k).head.v)

      //compute the z[k] element as the division of the accumulate value with l[k][k]
      val zk_ = z map {
        case MatrixElement(i, j, v) if (i == _k) => MatrixElement(i, j, v / lkk.value)
        case others => others
      }

      //if this is the last iteration return the computed z vector
      if (_k == n) zk_
      //if not, parallel reduce z[k] from the matrix
      else {
        //get z[k]
        val zkk = sc.broadcast(zk_.where($"i" === k).head.v)

        //multiply the k column of the l matrix with z[k]
        val lkx = l filter { $"j" === k && $"i" > k } map {
          case MatrixElement(i, j, lij) => MatrixElement(i, j, lij * zkk.value)
        }

        //subtract from the acumulator the l[k] * z[k] column
        val _zk = zk_.joinWith(
          lkx,
          zk_.col("i") === lkx.col("i") && zk_.col("i") > k,
          "left_outer") map {
          case (zki, null) => zki
          case (MatrixElement(i, j, zki), MatrixElement(_, _, lkxi)) => MatrixElement(i, j, zki - lkxi)
        }
        val zk = _zk.repartition(numpar, $"i").rdd.setName(s"z${_k}").localCheckpoint().toDS

        //iterate
        progressiveSubstitution(zk, _k + 1L)
      }
    }

    //compute z from Lz = b
    val z = progressiveSubstitution(b, 0L)

    //functional implementation of the regressive substitution process using a tail recursive function
    /**
      *
      * @param x represent b , the calculus of Lz = b
      * @param _k iteration
      * @return x , the answer to de system
      */
    @tailrec
    def regressiveSubstitution(x: Matrix, _k: Long): Matrix = {
      //create a literal column with the value of the actual iteration
      val k = sqlfun.lit(_k)

      //get the u[k][k] element
      val ukk = sc.broadcast(u.where($"i" === k && $"j" === k).head.v)

      //compute the x[k] element as the division of the accumulate value with u[k][k]
      val xk_ = x map {
        case MatrixElement(i, j, v) if (i == _k) => MatrixElement(i, j, v / ukk.value)
        case others => others
      }

      //if this is the last iteration return the computed x vector
      if (_k == 0L) xk_
      //if not, parallel reduce x[k] from the matrix
      else {
        //get x[k]
        val xkk = sc.broadcast(xk_.where($"i" === k).head.v)

        //multiply the k column of the u matrix with x[k]
        val ukx = u filter { $"j" === k && $"i" < k } map {
          case MatrixElement(i, j, uij) => MatrixElement(i, j, uij * xkk.value)
        }

        //subtract from the acumulator the u[k] * x[k] column
        val _xk = xk_.joinWith(
          ukx,
          xk_.col("i") === ukx.col("i") && xk_.col("i") < k,
          "left_outer") map {
          case (xki, null) => xki
          case (MatrixElement(i, j, xki), MatrixElement(_, _, ukxi)) => MatrixElement(i, j, xki - ukxi)
        }
        val xk = _xk.repartition(numpar, $"i").rdd.setName(s"x${_k}").localCheckpoint().toDS

        //iterate
        regressiveSubstitution(xk, _k - 1L)
      }
    }

    //compute x from Ux = z
    val x = regressiveSubstitution(z, n)

    //sort the computed solution
    val result = x.sort($"i").repartition(numpar).rdd.setName("x").localCheckpoint().toDS

    //return the computed solution of the system
    result
  }
}
