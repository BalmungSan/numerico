package funnum.parallel

//imports
import funnum.parallel.matrix._
import funnum.parallel.methods._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.{functions => sqlfun}
import scala.math.BigDecimal
import scala.util.Random

/** Application Entry Point */
object Main {
  /** Application usage string */
  private val usage = """spark-submit [SPARK_OPTIONS] \
                        #  --class "funnum.parallel.Main" [JAR] \
                        #  [--open=[PATH] | --create=[N]] \
                        #  [--gauss_elimination | --lu_gauss | --jacobi=[MAX_ITER,TOL,R,[absolute | relative]]] \
                        #  [--save=[all | x | error | none],[PATH]]""".stripMargin('#')

  /** Prints an error message and stop the application */
  private def error(msg: String)(implicit spark: SparkSession): Unit = {
    Console.err.println(s"Error: ${msg}")
    Console.err.println(s"Usage:\n${usage}")
    spark.stop()
    sys.exit(-1);
  }

  /** Main Method */
  def main(args: Array[String]): Unit = {
    //initialize the spark session
    val spark = SparkSession.builder.appName("Funnum Parallel").getOrCreate()
    val sc = spark.sparkContext
    import spark.implicits._
    implicit val _spark = spark

    //check that the application was called with the correct number of arguments
    if (args.size != 3) error("Bad number of arguments")

    //parse the first argument, open or randomly create the input matrices
    val open = """--open=(.*)""".r
    val create = """--create=(\d+)""".r
    val (a, b, xr, i) = args(0) match {
      case open(path) => {
        val a = matrix.loadMatrixFromHDFS(s"${path}/a")
        val b = matrix.loadMatrixFromHDFS(s"${path}/b")
        val x = matrix.loadMatrixFromHDFS(s"${path}/x")
        val i = matrix.loadMatrixFromHDFS(s"${path}/i")
        (a, b, x, i)
      }
      case create(_n) => {
        val n = _n.toLong
        val (a, b, xr) = matrix.randomLinearSystem(n, 3)
        val generator = new Random(n)
        val iData = for {
          i <- 0L until n
          r = generator.nextDouble() * 10
          sign = generator.nextBoolean()
          v = if (sign) -r else r
        } yield MatrixElement(i, n, v)
        val i = sc.parallelize(iData).toDS().cache()
        (a, b, xr, i)
      }
      case _ => error("Bad first argument"); (null, null, null, null)
    }

    //parse the second argument, apply one algorithm to the input matrices
    val jacobiArgs = """--jacobi=(\d+),([0-9e\\.\\-]+),([0-9e\\.\\-]+),(absolute|relative)""".r
    val xa = args(1) match {
      case "--gauss_elimination" => GaussElimination(a, b)
      case "--lu_gauss" => GaussFactorization(a, b)
      case jacobiArgs(_maxIter, _tol, _r, _error) => {
        val maxIter = _maxIter.toInt
        val tol = BigDecimal(_tol)
        val r = BigDecimal(_r)
        val error = if (_error == "absolute") true else false
        Jacobi(a, i, b, r)(maxIter, tol, error)
      }
      case _ => error("Bad second argument"); null
    }

    //compute the error associated with the algorithm
    val err = xr.join(
      xa,
      xr.col("i") === xa.col("i") && xr.col("j") === xa.col("j"),
      "inner").agg(sqlfun.max(sqlfun.abs(xr.col("v") - xa.col("v"))).cast("string"))

    //parse the third argument, save the results
    val saveOpts = """--save=(all|x|error|none),(.*)""".r
    args(2) match {
      case saveOpts("all", path)=> xa.save(s"${path}\\x"); err.write.text(s"${path}/error")
      case saveOpts("x", path)=> xa.save(s"${path}/x")
      case saveOpts("error", path)=> err.write.text(s"${path}/error")
      case saveOpts("none", _)=> {}
      case _ => error("Bad third argument"); {}
    }

    //stop the spark session and exit cleanly
    spark.stop()
  }
}
