package funnum.parallel

//Spark-SQL imports
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.types.{DecimalType, LongType, StructField, StructType}
//Scala imports
import scala.math
import scala.math.BigDecimal //128-bit signed floating point numbers (34 digits precision)
import scala.util.Random //random number generator
//make the implicit value scala.language.implicitConversions visible
//to enable the implicit conversions in this module
import scala.language.implicitConversions


/** This package contains the basic abstraction of a '''distributed ''matrix''''' and its elements,
  * its basic ''algorithms'' such as __parallel__ addition and multiplication of matrices,
  * generation of __random__ linear systems, and ''save & load''  matrices in '''HDFS'''
  */
package object matrix {
  /** Each ''matrix'' element is composed of two [[scala.Long Longs]]
    * that represent the ''i'' and ''j'' indexes of the element
    * and a [[scala.math.BigDecimal BigDecimal]] holding the '''value''' of the element
    *
    * @param i the row index
    * @param j the column index
    * @param v the value of this element
    *
    * @see [[scala.math.BigDecimal BigDecimal]]
    */
  case class MatrixElement(i: Long, j: Long, v: BigDecimal)

  /** Type alias of a '''distributed ''matrix''''' as a
    * [[org.apache.spark.sql.Dataset Dataset]] of
    * [[funnum.parallel.matrix.MatrixElement matrix elements]]
    *
    * @see [[funnum.parallel.matrix.MatrixElement MatrixElement]]
    * @see [[org.apache.spark.sql.Dataset Dataset]]
    */
  type Matrix = Dataset[MatrixElement]

  /** Randomly generate a linear equation system
    *
    * Generates a random __diagonal dominant__ matrix of size '''n * n''' ''A''
    * as the coefficient matrix of the system,
    * then generates a random ''x'' vector of size '''n'''
    * as a solution of the system,
    * and finally computes the ''b'' vector of size '''n'''
    * by multiplying the ''A'' matrix with the ''x'' vector
    *
    * @param n the size of the linear system
    * @param seed the seed for the random generator, defaults to zero
    * @return a [[scala.Tuple3 tuple]] where
    * the first element is the a matrix,
    * the second is the b vector,
    * and the third is the x vector
    *
    * @note this method is deterministic, given the same inputs the output will be always the same
    *
    * @see [[org.apache.spark.sql.SparkSession SparkSession]]
    */
  def randomLinearSystem(n: Long, seed: Long = 0L)(implicit spark: SparkSession): (Matrix, Matrix, Matrix) = {
    //import the spark implicits
    import spark.implicits._
    val sc = spark.sparkContext

    //create a random a matrix of size n * n
    val a = (sc.parallelize(0L until n) map {
      i => (i, 0L until n)
    } flatMap  {
      //fill each row with random values
      case (i, iter) => {
        val generator = new Random(seed + i)
        val values = for {
          j <- iter
          r = generator.nextDouble() * 10
          sign = generator.nextBoolean() //true means negative, false positive
          v = if (sign) -r else r
        } yield (j, v)
        //compute the value of teh diagonal as the absolute sum of all the elements in the row plus one
        val d = values.map(x => math.abs(x._2)).sum + 1.0d
        //update the value of the diagonal
        values map { case (j, v) => if (j == i) MatrixElement(i, j, d) else MatrixElement(i, j, v) }
      }
    }).toDS().cache()

    //create a random x vector of size n
    val generator = new Random(seed + n)
    val xData = for {
      i <- 0L until n
      r = generator.nextDouble() * 10
      sign = generator.nextBoolean() //true means negative, false positive
      v = if (sign) -r else r
    } yield MatrixElement(i, n, v)
    val x = sc.parallelize(xData).toDS().cache()

    //computes the vector b as the matrix product between a and x
    val b = parMatrixMultiplication(a, x).cache()

    //return the a matrix and the b and x vectors
    (a, b, x)
  }

  /** Returns the addition of two [[funnum.parallel.matrix.Matrix Distributed Matrices]]
    * @see [[funnum.parallel.matrix.Matrix Matrix]]
    * @see [[org.apache.spark.sql.SparkSession SparkSession]]
    */
  def parMatrixAddition(m1: Matrix, m2: Matrix)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._

    m1.joinWith(
      m2,
      m1.col("i") === m2.col("i") && m1.col("j") === m2.col("j"),
      "inner"
    ) map {
      case (MatrixElement(i1, j1, v1), MatrixElement(i2, j2, v2)) => MatrixElement(i1, j2, v1 + v2)
    }
  }

  /** Returns the subtraction of two [[funnum.parallel.matrix.Matrix Distributed Matrices]]
    * @see [[funnum.parallel.matrix.Matrix Matrix]]
    * @see [[org.apache.spark.sql.SparkSession SparkSession]]
    */
  def parMatrixSubtraction(m1: Matrix, m2: Matrix)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._

    m1.joinWith(
      m2,
      m1.col("i") === m2.col("i") && m1.col("j") === m2.col("j"),
      "inner"
    ) map {
      case (MatrixElement(i1, j1, v1), MatrixElement(i2, j2, v2)) => MatrixElement(i1, j2, v1 - v2)
    }
  }

  /** Returns the matricial multiplication of two [[funnum.parallel.matrix.Matrix Distributed Matrices]]
    * @see [[funnum.parallel.matrix.Matrix Matrix]]
    * @see [[org.apache.spark.sql.SparkSession SparkSession]]
    */
  def parMatrixMultiplication(m1: Matrix, m2: Matrix)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._

    m1.joinWith(
      m2,
      m1.col("j") === m2.col("i"),
      "inner"
    ) map {
      case (MatrixElement(i, _, v1), MatrixElement(_, j, v2)) => (i, j, v1 * v2)
    } groupByKey {
      case (i, j, v) => (i, j)
    } reduceGroups {
      (a, b) => (a, b) match { case ((_, _, v1), (_, _, v2)) => (0L, 0L, v1 + v2) }
    } map {
      case ((i, j), (_, _, v)) => MatrixElement(i, j, v)
    }
  }

  /** Returns the divition of two [[funnum.parallel.matrix.Matrix Distributed Matrices]]
    * @see [[funnum.parallel.matrix.Matrix Matrix]]
    * @see [[org.apache.spark.sql.SparkSession SparkSession]]
    */
  def parMatrixDivition(m1: Matrix, m2: Matrix)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._

    m1.joinWith(
      m2,
      m1.col("i") === m2.col("i") && m1.col("j") === m2.col("j"),
      "inner"
    ) map {
      case (MatrixElement(i1, j1, v1), MatrixElement(i2, j2, v2)) => MatrixElement(i1, j2, v1 / v2)
    }
  }

  /** Returns the linear multiplication of two [[funnum.parallel.matrix.Matrix Distributed Matrices]]
    * @see [[funnum.parallel.matrix.Matrix Matrix]]
    * @see [[org.apache.spark.sql.SparkSession SparkSession]]
    */
  def parVectMultiplication(m1: Matrix, m2: Matrix)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._

    m1.joinWith(
      m2,
      m1.col("i") === m2.col("i") && m1.col("j") === m2.col("j"),
      "inner"
    ) map {
      case (MatrixElement(i1, j1, v1), MatrixElement(i2, j2, v2)) => MatrixElement(i1, j2, v1 * v2)
    }
  }

  /** Returns the transpose of a [[funnum.parallel.matrix.Matrix Distributed Matrix]]
    * @see [[funnum.parallel.matrix.Matrix Matrix]]
    * @see [[org.apache.spark.sql.SparkSession SparkSession]]
    */
  def parMatrixTranspose(m: Matrix)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._

    m map {
      case MatrixElement(i, j, v) => MatrixElement(j, i, v)
    }
  }

  //matrix options and schema for writing and reading a matrix in HDFS
  private[matrix] val matrixOptions = Map("header" -> "true", "compression" -> "snappy")
  private[matrix] val matrixSchema = StructType(Seq(
    StructField("i", LongType, false),
    StructField("j", LongType, false),
    StructField("v", DecimalType.SYSTEM_DEFAULT, false)))

  /** Saves a [[funnum.parallel.matrix.Matrix Distributed Matrices]] in a '''HDFS''' folder
    *
    * The matrix is saved as a '''__snappy__ compressed ''csv''''' file with a header line,
    * where each line is composed of the ''i,j'' indexes of a the element as [[scala.Long Longs]]
    * and the [[scala.Predef.String String]] representation of a [[scala.math.BigDecimal BigDecimal]]
    * as the value of the element
    *
    * @param m the matrix to save
    * @param path the path where the matrix will be saved
    *
    * @see [[funnum.parallel.matrix.Matrix Matrix]]
    * @see [[org.apache.spark.sql.DataFrameWriter DataFrameWriter]]
    */
  def saveMatrixInHDFS(m: Matrix, path: String): Unit = m.write.options(matrixOptions).csv(path)

  /** Reads a [[funnum.parallel.matrix.Matrix Distributed Matrices]] from a '''HDFS''' folder
    *
    * The matrix must be a '''__snappy__ compressed ''csv''''' file with a header line,
    * where each line is composed of the ''i,j'' indexes of a the element as [[scala.Long Longs]]
    * and the [[scala.Predef.String String]] representation of a [[scala.math.BigDecimal BigDecimal]]
    *
    * @param path the path where the matrix is stored
    * @return the readed matrix
    *
    * @see [[funnum.parallel.matrix.Matrix Matrix]]
    * @see [[org.apache.spark.sql.DataFrameReader DataFrameReader]]
    * @see [[org.apache.spark.sql.SparkSession SparkSession]]
    */
  def loadMatrixFromHDFS(path: String)(implicit spark: SparkSession): Matrix = {
    //import the spark implicits
    import spark.implicits._
    spark.read.options(matrixOptions).schema(matrixSchema).csv(path).as[MatrixElement]
  }

  /** Wrapper class for using the parallel matrix operations in an object oriented manner */
  class MatrixOps(self: Matrix)(implicit spark: SparkSession) {
    def +(other: Matrix): Matrix = parMatrixAddition(self, other)
    def -(other: Matrix): Matrix = parMatrixSubtraction(self, other)
    def *(other: Matrix): Matrix = parMatrixMultiplication(self, other)
    def /(other: Matrix): Matrix = parMatrixDivition(self, other)
    def x(other: Matrix): Matrix = parVectMultiplication(self, other)
    def t: Matrix = parMatrixTranspose(self)
    def save(path: String): Unit = saveMatrixInHDFS(self, path)
  }

  /** Implicit conversion between a Matrix and a MatrixOps wrapper class */
  implicit def matrix2matrixOps(m: Matrix)(implicit spark: SparkSession): MatrixOps = new MatrixOps(m)(spark)
}
