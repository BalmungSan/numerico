#!/usr/bin/env python3

# import numpy for using floats of 32, 64 and 128 bits
import numpy as np

def single_1():
  """
  Computes the epsilon of the machine
  using single precision floats
  and the comparison method 1 != 1 + D
  """
  d = np.float32(0.5)
  two = np.float32(2.0)
  one = np.float32(1.0)
  while (one + d) != one:
    print(d)
    d = d / two

def single_0():
  """
  Computes the epsilon of the machine
  using single precision floats
  and the comparison method 0 != D
  """
  d = np.float32(0.5)
  two = np.float32(2.0)
  zero = np.float32(0.0)
  while d != zero:
    print(d)
    d = d / two

def double_1():
  """
  Computes the epsilon of the machine
  using double precision floats
  and the comparison method 1 != 1 + D
  """
  d = np.float64(0.5)
  two = np.float64(2.0)
  one = np.float64(1.0)
  while (one + d) != one:
    print(d)
    d = d / two

def double_0():
  """
  Computes the epsilon of the machine
  using double precision floats
  and the comparison method 0 != D
  """
  d = np.float64(0.5)
  two = np.float64(2.0)
  zero = np.float64(0.0)
  while d != zero:
    print(d)
    d = d / two

def quad_1():
  """
  Computes the epsilon of the machine
  using quadruple precision floats
  and the comparison method 1 != 1 + D
  """
  d = np.float128(0.5)
  two = np.float128(2.0)
  one = np.float128(1.0)
  while (one + d) != one:
    print(d)
    d = d / two

def quad_0():
  """
  Computes the epsilon of the machine
  using quadruple precision floats
  and the comparison method 0 != D
  """
  d = np.float128(0.5)
  two = np.float128(2.0)
  zero = np.float128(0.0)
  while d != zero:
    print(d)
    d = d / two

def main():
  """ Main Method """
  message = "Seleccione la acción que quieres realizar:\n"\
            " * s1: para calcular el epsilon de la máquina usando precisión simple y el método 1 != 1 + d\n"\
            " * s0: para calcular el epsilon de la máquina usando precisión simple y el método 0 != d\n"\
            " * d1: para calcular el epsilon de la máquina usando precisión doble y el método 1 != 1 + d\n"\
            " * d0: para calcular el epsilon de la máquina usando precisión doble y el método 0 != d\n"\
            " * q1: para calcular el epsilon de la máquina usando precisión cuadruple y el método 1 != 1 + d\n"\
            " * q0: para calcular el epsilon de la máquina usando precisión cuadruple y el método 0 != d\n"\
            " * e: para salir\n"
  run = True
  while run:
    c = input(message)
    if c == "e":
      #exit
      run = False
    elif c == "s1":
      #single 1 != 1 + d
      single_1()
    elif c == "s0":
      #single 0 != d
      single_0()
    elif c == "d1":
      #double 1 != 1 + d
      double_1()
    elif c == "d0":
      #double 0 != d
      double_0()
    elif c == "q1":
      #quadruple 1 != 1 + d
      quad_1()
    elif c == "q0":
      #quadruple 0 != d
      quad_0()
    else:
      #error
      print("Comando Invalido:", c)

#start the app
if __name__ == "__main__":
  main()
