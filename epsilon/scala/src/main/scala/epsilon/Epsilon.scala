package epsilon

//import the tailrec annotation
import scala.annotation.tailrec
//
import scala.io.StdIn

/** Application Entry Point
 * This application provide different methods
 * for calculating the epsilon of the machine
 */
object Epsilon extends App {
  //
  @tailrec
  private def do_until[T: Fractional](x: T, two: T, condition: T => Boolean): T = {
    import Fractional.Implicits._
    if (!condition(x)) x
    else {
      println(x)
      do_until(x / two, two, condition)
    }
  }

  /** Computes the epsilon of the machine
   * using single precision floats
   * and the comparision method 1 != 1 + D
   */
  def single_1() = {
    val d = 0.5f
    val two = 2.0f
    val one = 1.0f
    do_until(d, two, (x: Float) => one != (x + one))
  }

  /** Computes the epsilon of the machine
   * using single precision floats
   * and the comparision method 0 != D
   */
  def single_0() = {
    val d = 0.5f
    val two = 2.0f
    val zero = 0.0f
    do_until(d, two, (x: Float) => zero != x)
  }

  /** Computes the epsilon of the machine
   * using double precision floats
   * and the comparision method 1 != 1 + D
   */
  def double_1() = {
    val d = 0.5d
    val two = 2.0d
    val one = 1.0d
    do_until(d, two, (x: Double) => one != (x + one))
  }

  /** Computes the epsilon of the machine
   * using double precision floats
   * and the comparision method 0 != D
   */
  def double_0() = {
    val d = 0.5d
    val two = 2.0d
    val zero = 0.0d
    do_until(d, two, (x: Double) => zero != x)
  }

  /** Computes the epsilon of the machine
   * using quadruple precision floats
   * and the comparision method 1 != 1 + D
   */
  def quad_1() = {
    //val d = BigDecimal(0.5)
    //val two = BigDecimal(2.0)
    //val one = BigDecimal(1.0)
    val d = BigDecimal.valueOf(0.5)
    val two = BigDecimal.valueOf(2.0)
    val one = BigDecimal.valueOf(1.0)
    do_until(d, two, (x: BigDecimal) => one != (one + x).rounded)
  }

  /** Computes the epsilon of the machine
   * using quadruple precision floats
   * and the comparision method 0 != D
   */
  def quad_0() = {
    val d = BigDecimal(0.5)
    val two = BigDecimal(2.0)
    val zero = BigDecimal(0.0)
    do_until(d, two, (x: BigDecimal) => zero != x.rounded)
  }

  //main
  var run = true
  while (run) {
    val c = StdIn.readLine(
      "Seleccione la acción que quieres realizar:\n" +
      " * s1: para calcular el epsilon de la máquina usando precisión simple y el método 1 != 1 + d\n" +
      " * s0: para calcular el epsilon de la máquina usando precisión simple y el método 0 != d\n" +
      " * d1: para calcular el epsilon de la máquina usando precisión doble y el método 1 != 1 + d\n" +
      " * d0: para calcular el epsilon de la máquina usando precisión doble y el método 0 != d\n" +
      " * q1: para calcular el epsilon de la máquina usando precisión cuadruple y el método 1 != 1 + d\n" +
      " * q0: para calcular el epsilon de la máquina usando precisión cuadruple y el método 0 != d\n" +
      " * e: para salir\n"
    )
    c match {
      case "e" => run = false  //exit
      case "s1" => single_1()  //single 1 != 1 + d
      case "s0" => single_0()  //single 0 != d
      case "d1" => double_1()  //double 1 != 1 + d
      case "d0" => double_0()  //double 0 != d
      case "q1" => quad_1()    //quadruple 1 != 1 + d
      case "q0" => quad_0()    //quadruple 0 != d
      case _ => println("Comando Invalido: ", c) //error
    }
  }
}
