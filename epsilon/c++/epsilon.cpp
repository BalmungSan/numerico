#include <iostream> //cout, cerr
#include <map>
using namespace std;

//commands
enum class Commands {DEFAULT, EXIT, S1, S0, D1, D0, Q1, Q0};
map<string,Commands> commands = {{"e", Commands::EXIT},
                                 {"s1", Commands::S1},
                                 {"s0", Commands::S0},
                                 {"d1", Commands::D1},
                                 {"d0", Commands::D0},
                                 {"q1", Commands::Q1},
                                 {"q0", Commands::Q0}};
/**
 * Computes the epsilon of the machine
 * using single precision floats
 * and the comparison method 1 != 1 + D
 */
  void single_1() {
    float d = 0.5;
    float two = 2.0;
    float one = 1.0;
    while (one != (d + one)) {
      cout << d << endl;
      d = d / two;
    }
  }

/**
 * Computes the epsilon of the machine
 * using single precision floats
 * and the comparison method 0 != D
 */
void single_0() {
  float d = 0.5;
  float two = 2.0;
  float zero = 0.0;
  while (zero != d) {
    cout << d << endl;
    d = d / two;
  }
}

/**
 * Computes the epsilon of the machine
 * using double precision floats
 * and the comparison method 1 != 1 + D
 */
void double_1() {
  double d = 0.5;
  double two = 2.0;
  double one = 1.0;
  while (one != (d + one)) {
    cout << d << endl;
    d = d / two;
  }
}

/**
 * Computes the epsilon of the machine
 * using double precision floats
 * and the comparison method 0 != D
 */
void double_0() {
  double d = 0.5;
  double two = 2.0;
  double zero = 0.0;
  while (zero != d) {
    cout << d << endl;
    d = d / two;
  }
}

/**
 * Computes the epsilon of the machine
 * using quadruple precision floats
 * and the comparison method 1 != 1 + D
 */
void quad_1() {
  long double d = 0.5L;
  long double two = 2.0L;
  long double one = 1.0L;
  while (one != (d + one)) {
    cout << d << endl;
    d = d / two;
  }
}

/**
 * Computes the epsilon of the machine
 * using quadruple precision floats
 * and the comparison method 0 != D
 */
void quad_0() {
  long double d = 0.5L;
  long double two = 2.0L;
  long double zero = 0.0L;
  while (zero != d) {
    cout << d << endl;
    d = d / two;
  }
}

/**
 * Main Method
 */
int main(int argc, char* argv[]) {
  string message = "Seleccione la acción que quieres realizar:\n\
  * s1: para calcular el epsilon de la máquina usando precisión simple y el método 1 != 1 + d\n\
  * s0: para calcular el epsilon de la máquina usando precisión simple y el método 0 != d\n\
  * d1: para calcular el epsilon de la máquina usando precisión doble y el método 1 != 1 + d\n\
  * d0: para calcular el epsilon de la máquina usando precisión doble y el método 0 != d\n\
  * q1: para calcular el epsilon de la máquina usando precisión cuadruple y el método 1 != 1 + d\n\
  * q0: para calcular el epsilon de la máquina usando precisión cuadruple y el método 0 != d\n\
  * e: para salir";
  bool run = true;
  string c;
  while (run) {
    cout << message << endl;
    getline(cin, c);
    switch (commands[c]) {
    case Commands::EXIT:
      run = false;
      break;
    case Commands::S1:
      //single 1 != 1 + d
      single_1();
      break;
    case Commands::S0:
      //single 0 != d
      single_0();
      break;
    case Commands::D1:
      //double 1 != 1 + d
      double_1();
      break;
    case Commands::D0:
      //double 0 != d
      double_0();
      break;
    case Commands::Q1:
      //quadruple 1 != 1 + d
      quad_1();
      break;
    case Commands::Q0:
      //quadruple 0 != d
      quad_0();
      break;
    case Commands::DEFAULT:
      //error
      cerr << "Comando Invalido: " << c << endl;
    }
  }
  return 0;
}
