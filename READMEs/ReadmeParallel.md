# Functional Parallel Numerical
Spark Application written in Scala that solve Big Systems of Linear Equation in parallel 

## Implemented methods

* GaussElimination
* LU Factorization by Gauss Elimination
* Jacobi

## Prerequisites

* sbt
* apache spark
 
## How to run the Functional Numerical Project?

```shell
$ sbt package 
$ spark-submit [SPARK_OPTIONS] \
  --class "funnum.parallel.Main" [JAR] \
  [--open=[PATH] | --create=[N]] \
  [--gauss_elimination | --lu_gauss | --jacobi=[MAX_ITER,TOL,R,[absolute | relative]]] \
  [--save=[all | x | error | none],[PATH]]
```

## Software

* The Scala Programming Language
 + A multiparadigm language that mix the best of the functional with the object oriented paradigms, which allows you to build elegant and scalable code.
* The Apache Spark Framework
 + “Is a fast and general engine for large-scale data processing” written in scala which allows its users to express complex implicit parallel computations over big datasets in an functional style.
 
## Links

* [Scala](http://scala-lang.org/) - Scala Programming Language (École Polytechnique Fédérale).
* [Spark](https://spark.apache.org/) - Lightning-fast cluster computing engine.
* [Functional Parallel Numerical API](https://balmungsan.gitlab.io/numerico/api/funnum/parallel/#funnum.parallel.package) - The generated Scaladoc for the Spark project.
