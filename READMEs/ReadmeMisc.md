# Functional Numerical _(Misc)_
Several scripts with topics related to numerical analysis.

## Prerequisites

* Python 3 with NumPy
* Gcc-g++ >= 4.8.5
* Scala at least 2.11

## Folder structure and how to run each code

* **epsilon.cpp** - C++ implementation of the algorithm to find the epsilon of the machine.
```shell
$ g++ --std=c++11 -o epsilon epsilon.cpp
$ ./epsilon
```

* **epsilon.py** - Python implementation of the algorithm to find the epsilon of the machine.
```shell
$ python epsilon.py
```

* **Epsilon.scala** - Scala implementation of the algorithm to find the epsilon of the machine.
```shell
$ scalac Epsilon.scala # Compile the code in Scala
$ scala Epsilon # Run the resulting and then, write the option
```

* **Error.scala** - Scala implementation of the error formulas.
```shell
$ scalac Error.scala # Compile the code in Scala
$ scala Error # Run the resulting and then, write the option
```

* **MatrixOps.scala** - Parallel matrix addition and multiplication in scala _(threads)_.
```shell
$ scalac MatrixOps.scala 
$ scala MatrixOps 
```

* **Taylor.scala** - Functional implementation of Taylor's series evaluator and combinators.
```shell
$ scalac Taylor.scala
$ scala Taylor
```
