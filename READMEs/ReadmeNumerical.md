# Functional Numerical _(Bonus)_
You will find the implementations of the methods viewed in class, which were developed with Scala a functional programming language, in the same way, the project have a useful web graphic interface to execute the methods.

## Prerequisites

* apache2
* sbt

## Implemented methods

* **Incremental Search:**
* **Solution of one variable equation:**
  + Bisection 
  + FalsePosition
  + FixedPoint
  + Newton
  + Secant
  + MultipleRoot
* **System of lineal equations**
  + GaussElimination
  + LUDecomposition
    - Methods that calculates the permuted matrix and the triangular matrices L and U
    - Cholesky
    - Doolittle
    - Crout
  + Jacobi
 
## How to run the Functional Numerical Project

```shell
$ sudo service apache2 start
$ sbt fullOptJS
$ sudo cp numerico/methods/index-opt.html /var/www/html/index.html
$ sudo cp numerico/methods/target/scala-2.12/*.js* /var/www/html/
```
Right now you can access to the application by entering to localhot in a web browser

## Links

* [Functional Numerical Methods API](https://balmungsan.gitlab.io/numerico/api/funnum/serial/funnum/methods/index.html) - The generated Scaladoc for
the Scala.js project
* [Functional Numerical Methods App](https://balmungsan.gitlab.io/numerico/app/) - Online JS Application generated from the project
* [Evaluator Syntax](http://mathjs.org/docs/expressions/syntax.html) - The especification of the syntax allowed in the evaluator
