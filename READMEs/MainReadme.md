# Functional Parallel Numerical
Research in the field of implementing **Parallel** _Numerical Methods_ for solving Big Systems of Linear Equations in a **Functional Style**.

### Authors

* Alcaraz Florez Juan Pablo
* Cordoba Bodhert Alejandro
* Mejía Suárez Luis Miguel
* Quinhonez Avila Jhonatan
* Salas Gonzales Wilfer Manuel

## What will you find in this project?

The following work shows the development of three Numerical Methods for solving Systems of Linear Equations, to be precise Gauss Elimination, LU Factorization by Gauss Elimination and Jacobi, in a Functional and Parallel way using The Spark processing engine with the Scala Programming Language.

## Project structure

* **FunctionalParallelNumerical**
 + This folder contains the results of the research, including all the source code and the user manual of the spark application. 
* **FunctionalNumerical**
 + Bonus to the investigation, we develop a Web Application with several numerical methods programmed using the functional paradigm.
* **_Misc_**
 + This folder contains several scripts with topics related to numerical analysis.
* **InvestigationReport.pdf:** LaTex document with the report of the Functional Parallel Numerical Methods Project.
* **FinalPresentation.pptx:** The slides used in the final presentation of the project.
* **MainReadme.pdf:** This file itself.
  
## Links

* [Investigation Report](https://www.overleaf.com/read/fqxdwgyjcctc#/41845395/)
* [Bonus Report](https://www.overleaf.com/read/hnqpnddjybzz)
* [Project Google Site](https://sites.google.com/site/proyectodeinvestigacionan/)
* [Project Repository](https://gitlab.com/BalmungSan/numerico.git)
